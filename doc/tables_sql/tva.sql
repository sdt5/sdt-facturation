BEGIN;

DROP TABLE IF EXISTS tva;

CREATE TABLE tva (
    id SERIAL PRIMARY KEY,
    taux INT NOT NULL,
    name VARCHAR(255) NOT NULL
);

INSERT INTO tva(taux, name) 
VALUES (
    '20', 'normal'
);

COMMIT;