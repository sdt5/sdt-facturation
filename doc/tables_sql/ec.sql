BEGIN;

DROP TABLE IF EXISTS ec;

CREATE TABLE ec (
    id SERIAL PRIMARY KEY,
    libelle VARCHAR(255) NOT NULL,
    date_debut DATETIME NOT NULL,
    date_fin DATETIME NOT NULL,
    site_id BIGINT UNSIGNED NOT NULL,
    FOREIGN KEY (site_id) REFERENCES site(id)
);

INSERT INTO ec(libelle, date_debut, date_fin, site_id) 
VALUES (
    '01/01/2021 - 31/12/2021', '2021-01-01 00:00', '2021-12-31 00:00', '1'
);

COMMIT;