BEGIN;

DROP TABLE IF EXISTS ligne_texte;

CREATE TABLE ligne_texte (
    id SERIAL PRIMARY KEY,
    texte TEXT,
    ligne_id BIGINT UNSIGNED NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP,
    FOREIGN KEY (ligne_id) REFERENCES ligne(id)
    ON DELETE CASCADE
);

INSERT INTO ligne_texte(texte, ligne_id) 
VALUES (
    'Un petit texte pour la 1ere ligne de PN', '150'
);

COMMIT;

