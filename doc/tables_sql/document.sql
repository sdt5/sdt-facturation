BEGIN;

DROP TABLE IF EXISTS document;

CREATE TABLE document (
    id SERIAL PRIMARY KEY,
    number VARCHAR(255) NOT NULL,
    client_id BIGINT UNSIGNED NOT NULL,
    type_id BIGINT UNSIGNED NOT NULL,
    user_id BIGINT UNSIGNED NOT NULL,
    site_id BIGINT UNSIGNED NOT NULL,
    statut_id BIGINT UNSIGNED NOT NULL,
    revision_number INTEGER DEFAULT 0,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP,
    FOREIGN KEY (client_id) REFERENCES client(id),
    FOREIGN KEY (type_id) REFERENCES type(id),
    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (site_id) REFERENCES site(id),
    FOREIGN KEY (statut_id) REFERENCES statut(id)
);

INSERT INTO document(number, client_id, type_id, user_id, site_id, statut_id) 
VALUES (
    'TOULOUSE-DEVIS-2021-001', '1', '1', '1', '1', '1'
);

COMMIT;