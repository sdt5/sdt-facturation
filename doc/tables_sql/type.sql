BEGIN;

DROP TABLE IF EXISTS type;

CREATE TABLE type (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    origin BOOLEAN NOT NULL DEFAULT 0,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP
);

INSERT INTO type(name, origin) 
VALUES (
    'devis', '1'
);

INSERT INTO type(name, origin) 
VALUES (
    'facture', '1'
);

INSERT INTO type(name, origin) 
VALUES (
    'devis et facture', '1'
);

COMMIT;