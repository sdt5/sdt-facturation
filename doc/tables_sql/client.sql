BEGIN;

DROP TABLE IF EXISTS client;

CREATE TABLE client (
    id SERIAL PRIMARY KEY,
    compagny VARCHAR(255) NOT NULL,
    contact_name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    tel INT NOT NULL,
    adress TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    logo VARCHAR(255), 
    updated_at TIMESTAMP,
    FOREIGN KEY (site_id) REFERENCES site(id)
);

INSERT INTO client(compagny, contact_name, email, tel, adress) 
VALUES (
    'Airbus', 'John Doe', 'john.doe@gmail.com', '0662069077', 'Akihabara, Tokyo'
);

COMMIT;