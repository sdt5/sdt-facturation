BEGIN;

DROP TABLE IF EXISTS statut;

CREATE TABLE statut (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    origin BOOLEAN DEFAULT 0,
    type_id BIGINT UNSIGNED NOT NULL,
    FOREIGN KEY (type_id) REFERENCES type(id)
);

INSERT INTO statut(name, origin, type_id) 
VALUES (
    'En création', '1', '3'
);

INSERT INTO statut(name, origin, type_id) 
VALUES (
    'Accepté', '1', '1'
);

INSERT INTO statut(name, origin, type_id) 
VALUES (
    'Envoyé', '1', '3'
);

INSERT INTO statut(name, origin, type_id) 
VALUES (
    'Refusé', '1', '1'
);

INSERT INTO statut(name, origin, type_id) 
VALUES (
    'En cours', '1', '3'
);

INSERT INTO statut(name, origin, type_id) 
VALUES (
    'Archivé', '1', '3'
);

INSERT INTO statut(name, origin, type_id) 
VALUES (
    'Exporté en compta', '1', '2'
);

COMMIT;