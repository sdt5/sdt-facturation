BEGIN;

DROP TABLE IF EXISTS site;

CREATE TABLE site (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) UNIQUE NOT NULL,
    code VARCHAR(255) UNIQUE NOT NULL,
    note TEXT,
    increment INT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP
);

INSERT INTO site(name, code, note, increment) 
VALUES (
    'Paris', 'PARIS', 'La plus belle note ever', '0'
);

COMMIT;