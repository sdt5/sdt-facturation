BEGIN;

DROP TABLE IF EXISTS ligne;

CREATE TABLE ligne (
    id SERIAL PRIMARY KEY,
    quantity INT NOT NULL DEFAULT 1,
    total_ht_brut INT NOT NULL,
    remise INT NOT NULL DEFAULT 0,
    prix_ht INT NOT NULL,
    prix_ttc INT NOT NULL,
    prix_remise INT NOT NULL,
    tva_id BIGINT UNSIGNED NOT NULL,
    pn_id VARCHAR(255) NOT NULL,
    pn_description VARCHAR(255),
    pn_name VARCHAR(255),
    document_id BIGINT UNSIGNED NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP,
    FOREIGN KEY (tva_id) REFERENCES tva(id)
);

INSERT INTO ligne(quantity, total_ht_brut, prix_ht, prix_ttc, remise, prix_remise, tva_id, pn_id, document_id) 
VALUES (
    '1', '100', '100', '120', '120', '0', '100', '1', '1'
);

COMMIT;
