# Dictionnaire de données #

## Users ##
Nom du champ|Type|
|---|---|---|
name|VARCHAR, NOT NULL, UNIQUE|
email|VARCHAR, NOT NULL|
pin|NUMBER, NOT NULL|

## Clients ##
Nom du champ|Type|
|---|---|---|
compagny|VARCHAR, NOT NULL, UNIQUE|
contact_name|VARCHAR, NOT NULL|
email|VARCHAR, NOT NULL, UNIQUE|
tel|NUMBER, NOT NULL|
logo|VARCHARCH, NULL|
adress|TEXT, NOT NULL|

## Type ##
Nom du champ|Type|
|---|---|---|
name|VARCHAR, NOT NULL, UNIQUE|
origin|BOOL, NOT NULL|

## Devis/Factures ##
Nom du champ|Type|
|---|---|---|
number|VARCHAR, NOT NULL|
date|DATETIME, NOT NULL|
revision_number|NUMBER, NOT NULL|
client_id|NUMBER, NOT NULL|
type_id|NUMBER, NOT NULL|
user_id|NUMBER, NOT NULL|
site_id|NUMBER, NOT NULL|
lignes_id|NUMBER, NOT NULL|

## Sites ##
Nom du champ|Type|
|---|---|---|
name|VARCHAR, NOT NULL|
note|TEXT, NOT NULL|
code|NUMBER, NOT NULL, UNIQUE|

## Taux ##
Nom du champ|Type|
|---|---|---|
name|VARCHAR, NOT NULL|
taux|NUMBER, NOT NULL|

## Lignes ##
Nom du champ|Type|
|---|---|---|
prix_ht|NUMBER, NOT NULL|
quantity|NUMBER, NOT NULL|
total_ht_brut|Type|
remise|NUMBER, NOT NULL|
prix_ht_remise|Type|
prix_ttc|Type|
tva_id|Type|
pn_id|NUMBER, NOT NULL|

## Statuts ##
Nom du champ|Type|
|---|---|---|
name|VARCHAR, NOT NULL|
origin|BOOL, NOT NULL|
type_id|NUMBER, NOT NULL|