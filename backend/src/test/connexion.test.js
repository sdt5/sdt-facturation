describe('connexion', () => {
    
    test('variable_env', () => {

        require('dotenv').config({path: __dirname+'/../config/.env'})

        a = process.env.PORT
    
        expect(a).toBe("3303")
    })
    
    test('connection_db', async() => {

        require('dotenv').config({path: __dirname+'/../config/.env'})
    
        const knex = require('knex')({
            client: 'mysql',
            connection: {
              host : process.env.HOST,
              user : process.env.USERNAME,
              password : process.env.PASS,
              database : process.env.DB_NAME
            }
        })
    
        const user_test = await
             knex
            .from('user')
            .select('name')
            .where('id', '1')
            .then(user => { return user[0].name})
            .catch(err => console.log(err))
        
        expect(user_test).toBe("Zeldacarin")

    
    })
})

