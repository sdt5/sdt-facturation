const express = require('express');
const router = express.Router();

const userController = require('./controllers/userController');
const documentController = require('./controllers/documentController');
const siteController = require('./controllers/siteController');
const clientController = require('./controllers/clientController');
const statusController = require('./controllers/statusController');
const tvaController = require('./controllers/tvaController');
const ligneController = require('./controllers/ligneController');
const ecController = require('./controllers/ecController');
const typeController = require('./controllers/typeController');
const ligneTexteController = require('./controllers/ligneTexteController');
const pdfController = require('./controllers/pdfController');

// Création des routes

// User
router.route('/user/add').post(userController.add);
router.route('/user/delete').post(userController.delete);
router.route('/user/login/').post(userController.login);
router.route('/user/login/').get(userController.logged);
router.route('/list/user').get(userController.listUser);
router.route('/user/:userId').get(userController.findById);

// Document
router.route('/user/login/list/devis').get(documentController.listDevis);
router.route('/user/login/list/facture').get(documentController.listFacture);
router.route('/user/login/list/document/:siteID').get(documentController.listDocument);
router.route('/user/login/list/allDocument').get(documentController.listAllDocument);
router.route('/document/:idDocument').get(documentController.findDocumentById);
router.route('/document/add').post(documentController.addDocument);
router.route('/document/update/:idDocument').post(documentController.updateDocument);

// PDF
router.route('/document/:idDocument/pdf').get(pdfController.savePDF);
router.route('/document/:idDocument/send').get(pdfController.sendPdf);

// Site
router.route('/site/add').post(siteController.add);
router.route('/site/delete').post(siteController.delete);
router.route('/list/site').get(siteController.listSite);
router.route('/site/:idSite').get(siteController.findSiteById);

// Client
router.route('/list/client').get(clientController.listClient);
router.route('/client/:idClient').get(clientController.findById);
router.route('/client/add').post(clientController.add);
router.route('/client/delete').post(clientController.delete);
router.route('/client/update').post(clientController.update);
router.route('/client/site/:idSite').get(clientController.listClientBySite);

// Statut
router.route('/list/status').get(statusController.listStatus);
router.route('/status/add').post(statusController.add);
router.route('/status/delete').post(statusController.delete);

// TVA
router.route('/list/tva').get(tvaController.listTVA);
router.route('/tva/:idTVA').get(tvaController.findByID);
router.route('/tva/add').post(tvaController.add);
router.route('/tva/delete').post(tvaController.delete);

// Ligne
router.route('/ligne/:idDocument').get(ligneController.findLigneByDocumentId);
router.route('/ligne/:idLigne').get(ligneController.findLigneById);
router.route('/ligne/:idDocument/add').post(ligneController.addLigne);
router.route('/ligne/:idDocument/delete').post(ligneController.delete);
router.route('/ligne/:idLigne/update').post(ligneController.update);
router.route('/ligne/:idDocument/last').get(ligneController.findLastRawByIdDocument);

// Ligne_TEXTE
router.route('/ligneTexte/add/:idLigne').post(ligneTexteController.add);

// EC
router.route('/ec/:idEC').get(ecController.findBySite);

// Type
router.route('/list/type').get(typeController.listTypes);



module.exports = router;