// Connexion à la db
const db = require('../../config/db');
const jwt = require('jsonwebtoken');

require('dotenv').config({path:__dirname+'/config/.env'});

module.exports = {
    listTypes : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On récupère la liste des devis
                let listTypes= await db
                    .select('*')
                    .from('type')
                
                res.json({data: listTypes})
            }

        })
    }
}