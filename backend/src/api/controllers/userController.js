// Connexion à la db
const db = require('../../config/db');
const jwt = require('jsonwebtoken');

// if (process.env.NODE_ENV)

require('dotenv').config({path:`${__dirname}/config/.env.${process.env.NODE_ENV}`});


module.exports = {
    add : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        
        let name = (req.body.name);
        let email = req.body.email;
        let pin = req.body.pin;

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, datajwt) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } 


            // On vérifie que cet utilisateur n'existe pas déjà
            let userAlreadyExist = await db
                .select('*')
                .from('user')
                .where({name: name})
                .orWhere({email: email})

            if (userAlreadyExist.length !== 0) {
                res.json({error: "Cet utilisateur existe déjà"})
            } else {
                // On ajoute le nouvel utilisateur
                let { data } = await db('user')
                    .insert({name: name, email: email, pin: pin})
                
                res.json({data})
            }
            
        })
    },
    delete : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let idUser = Number(req.body.id);

        console.log(req.body)

        // On verifie que le client n'est pas présent dans un document avant de supprimer
        let userFound = await db
            .select('*')
            .from('document')
            .where({user_id: idUser})

        if(userFound.length !== 0) {
            res.json({error: "Cet utilisateur est présent dans un document, il ne peut être supprimer"})
        } else {
            if (token) {
                jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
                    if(err) {
                        res.json({error: "Vous n'avez pas accès à ces ressources"})
                    } else {
        
                        await db('user')
                            .where({id: idUser})
                            .del()                    
                        res.json({data: "Ce client a bien été supprimée"})
                    }
                })
            }
        }
    },
    listUser : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            console.log(data)
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On récupère la liste des utilisateurs
                let listUser = await db
                    .select('name', 'id', 'email')
                    .from('user')
                
                res.json({data: listUser})
            }
        })
    },

    findById : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let userId = req.params.userId;

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            console.log(data)
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On find l'utilisateur
                console.log(req.params.userId)
                let user = await db
                    .select('*')
                    .from('user')
                    .where({id: userId})
                
                if (user) {
                    res.json({data: user})
                } else {
                    res.json({error: "Cet utilisateur n'existe pas"})
                }
                
                
            }
        })
    },

    login : async(req, res) => {

        let name = req.body.name;
        let pin = Number(req.body.pin);
        let idSite = Number(req.body.site)


        try {

            let userFound = await db
                .select('name', 'pin', 'id')
                .from('user')
                .where({name: name})
                            
                if (userFound.length == 0 || userFound[0].pin != pin) {
                    res.json({error: "Vos identifiants sont inccorects. Veuillez vérifier votre Nom et code PIN."})
                }

                if(userFound && userFound[0].pin === pin) {
                    // Création du token lorsque l'utilisateur est authentifié
                    const jwtToken = jwt.sign(
                        {
                            id: userFound[0].id,
                            siteID: idSite
                        }, 
                        process.env.JWT_SIGN_SECRET
                    )
                    // Envoi du token
                    res.json({token: jwtToken})
                }

        } catch (error) {
            console.log(error)
        }   
    },

    logged : async (req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');

        if (token) {
            jwt.verify(token, process.env.JWT_SIGN_SECRET, (err, data) => {
                if(err) {
                    res.sendStatus(403)
                } else {
                    res.json({data: data})
                }
    
            })
        }
    }
}

