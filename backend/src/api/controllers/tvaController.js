// Connexion à la db
const db = require('../../config/db');
const jwt = require('jsonwebtoken');

require('dotenv').config({path:__dirname+'/config/.env'});

module.exports = {
    listTVA: async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On récupère la liste des devis
                let listTVA= await db
                    .select('*')
                    .from('tva')
                
                res.json({data: listTVA})
            }

        })
    },
    add: async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');

        let name = (req.body.name);
        let taux = req.body.taux;

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On vérifie que ce taux n'existe pas déjà
                let tauxAlreadyExist = await db
                .select('*')
                .from('tva')
                .where({name: name})

                if (tauxAlreadyExist.length !== 0) {
                    res.json({error: "Ce taux existe déjà"})
                } else {
                    // On ajoute le nouveau taux
                    let { data } = await db('tva')
                        .insert({taux: taux, name: name})
                    
                    res.json({data: "Ce taux a bien été ajouté"})
                }
            }

        })
    },
    delete : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let idTVA = req.body.id;

        // On verifie que le taux n'est pas présent dans un document (une ligne) avant de supprimer
        let tvaFound = await db
            .select('*')
            .from('ligne')
            .where({tva_id: idTVA})

        if(tvaFound.length !== 0) {
            res.json({error: "Cette TVA est présente dans un document, elle ne peut être supprimée"})
        } else {
            if (token) {
                jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
                    if(err) {
                        res.json({error: "Vous n'avez pas accès à ces ressources"})
                    } else {
        
                        await db('tva')
                            .where({id: idTVA})
                            .del()                    
                        res.json({data: "Cette TVA a bien été supprimée"})
                    }
                })
            }
        }
    },
    findByID: async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let idTVA = req.params.idTVA;

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On récupère la liste des devis
                let tva= await db
                    .select('*')
                    .from('tva')
                    .where({id: idTVA})
                
                res.json({data: tva})
            }
        })
    }
}