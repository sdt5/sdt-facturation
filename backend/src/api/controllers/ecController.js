const db = require('../../config/db');
const jwt = require('jsonwebtoken');

require('dotenv').config({path:__dirname+`/config/.env.${process.env.NODE_ENV}`});

module.exports = {
    findBySite: async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let id = req.params.idEC

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On récupère la liste des ec
                let lignes = await db
                    .select('*')
                    .from('ec')
                    // .innerJoin('document', 'document.id', 'document_id')
                    .where({site_id: id})
                
                res.json({data: lignes})
            }

        })
    }
}