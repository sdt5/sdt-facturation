const db = require('../../config/db');
const jwt = require('jsonwebtoken');

require('dotenv').config({path:__dirname+`/config/.env.${process.env.NODE_ENV}`});

module.exports = {
    findLigneByDocumentId : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let id = req.params.idDocument

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {

                let lignes = await db
                    .select('document.id', 'quantity', 'remise', 'total_ht_brut', 'prix_ht', 'prix_ttc', 'tva_id', 'pn_id', 'document_id', 'tva.name as tva_name', 'tva.taux as tva_taux', 'prix_remise', 'ligne.id as ligne_id', 'ligne_texte.texte', 'ligne.pn_description', 'ligne.pn_name')
                    .from('ligne')
                    .innerJoin('document', 'document.id', 'document_id')
                    .innerJoin('tva', 'tva.id', 'tva_id')
                    .leftJoin('ligne_texte', 'ligne_texte.ligne_id', 'ligne.id')
                    .where({document_id: id})
                
                res.json({data: lignes})
            }

        })
    },

    findLigneById : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let id = req.params.idLigne

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {

                let lignes = await db
                    .select('document.id', 'quantity', 'remise', 'total_ht_brut', 'prix_ht', 'prix_ttc', 'tva_id', 'pn_id', 'document_id', 'tva.name as tva_name', 'tva.taux as tva_taux', 'prix_remise', 'ligne.id as ligne_id', 'ligne_texte.texte', 'ligne.pn_description', 'ligne.pn_name')
                    .from('ligne')
                    .innerJoin('document', 'document.id', 'document_id')
                    .innerJoin('tva', 'tva.id', 'tva_id')
                    .leftJoin('ligne_texte', 'ligne_texte.ligne_id', 'ligne.id')
                    .where({id: id})
                
                res.json({data: lignes})
            }

        })
    },

    addLigne : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let id = req.params.idDocument;

        let quantity = Number(req.body.quantity);
        let prixHT = req.body.prix_unitaire_ht;
        let totalHT = req.body.prix_ht;
        let tvaID = req.body.tva
        let pnID = req.body.pn_id;
        let remise = req.body.remise;
        let prixTTC = req.body.prix_ttc;
        let prix_remise = req.body.prix_ht_remise;
        let pnDescription = req.body.pn_description;
        let pnName = req.body.pn_name

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {

                let lignes = await db('ligne')
                    .insert({
                        quantity: quantity, 
                        total_ht_brut: totalHT, 
                        remise: remise, 
                        prix_ht: prixHT, 
                        prix_ttc: prixTTC, 
                        prix_remise: prix_remise,
                        tva_id: tvaID, 
                        pn_id: pnID, 
                        document_id: id,
                        pn_description: pnDescription,
                        pn_name: pnName
                    })

                res.json({data: "Votre PN a bien été ajouté"})
                    
            }
        })
    },

    delete : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let id = Number(req.params.idDocument);
        let idLigne = Number(req.body.id_ligne);

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {

                let lignes = await db('ligne')
                    .where({id: idLigne})
                    .del()
                
                res.json({data: "La ligne a bien été supprimée"})
            }

        })
    },

    update : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let idLigne = req.body.idLigne;
        let field = req.body.field;
        let value = req.body.value

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {

                let ligne = await db('ligne')
                            .select('prix_ht', 'remise', 'tva.taux as tva_taux', 'prix_remise', 'quantity')
                            .innerJoin('tva', 'tva.id', 'tva_id')
                            .where({'ligne.id': idLigne})
                
                if (field === 'quantity') {
                    let total_ht_brut = value * ligne[0].prix_ht
                    let prix_remise = total_ht_brut * (1 - ligne[0].remise /100)
                    let prix_ttc = prix_remise * (1 + ligne[0].tva_taux / 100)

                    await db('ligne')
                    .update({quantity: value, total_ht_brut: total_ht_brut, prix_remise: prix_remise, prix_ttc: prix_ttc})
                    .where({'ligne.id': idLigne})

                    res.json({data: "Cette cellule a bien été modifiée"})
                } else if (field === 'tva') {
                    let tva = await db('tva')
                                .select('taux')
                                .where({id: value})
                    let prix_ttc = ligne[0].prix_remise * (1 + tva[0].taux / 100)

                    await db('ligne')
                    .update({prix_ttc: prix_ttc, tva_id: value})
                    .where({'ligne.id': idLigne})

                    res.json({data: "Cette cellule a bien été modifiée"})
                } else (
                    res.json({error: "Cette cellule ne peut être modifiée."})
                )

                
            }

        })
    },

    findLastRawByIdDocument : async(req, res) => {
        let id = Number(req.params.idDocument);

        let newRaw = await db('ligne')
                        .select('ligne.id as ligne_id', 'quantity', 'remise', 'total_ht_brut', 'prix_ht', 'prix_ttc', 'tva_id', 'pn_id', 'document_id', 'tva.name as tva_name', 'tva.taux as tva_taux', 'prix_remise', 'pn_name', 'pn_description')
                        .innerJoin('document', 'document.id', 'document_id')
                        .innerJoin('tva', 'tva.id', 'tva_id')
                        .where({document_id: id})
                        .orderBy('ligne.id', 'desc')
                        .limit(1)

                        res.json({data: newRaw})
    }
}