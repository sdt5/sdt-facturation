// Connexion à la db
const db = require('../../config/db');
const jwt = require('jsonwebtoken');
const dayjs = require('dayjs');

require('dotenv').config({path:__dirname+`/config/.env.${process.env.NODE_ENV}`});

module.exports = {
    addDocument : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        
        let number = (req.body.number);
        let idClient = Number(req.body.idClient);
        let idType = Number(req.body.idType);
        let idUser = Number(req.body.idUser);
        let idSite = Number(req.body.idSite);
        let idStatut = Number(req.body.idStatut);

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } 

            // Validation 
            if(typeof(number) != "string" || idClient === NaN || idType === NaN || idUser === NaN || idSite === NaN || idStatut === NaN) {
                res.json({error: "Veuillez vérifier les données saisies"})
            }

            // On ajoute le nouveau document
            let newDocument = await db('document')
                .insert({number: number, client_id: idClient, type_id: idType, user_id: idUser, site_id: idSite, statut_id: idStatut})

            await db('site')
                .increment({increment: 1})
                .where({id: idSite})
            
            res.json({data: "Votre document a bien été créé"})
        })
    },

    updateDocument : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let now = dayjs();
        
        let idStatut = Number(req.body.idStatut);
        let idDocument = Number(req.params.idDocument);
        let updatedAt = now.$d;

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } 

            // Validation 
            if(idDocument === NaN || idStatut === NaN) {
                res.json({error: "Veuillez vérifier les données saisies"})
            }

            // On met à jour le document
            await db('document')
                .update({statut_id: idStatut, updated_at: updatedAt})
                .where({id: idDocument})
            
            res.json({data: "Votre document a bien été modifié"})
        })
    },

    listDevis : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On récupère la liste des devis
                let listDevis = await db
                    .select('*')
                    .from('document')
                    .innerJoin('client', 'client.id')
                    .where({user_id: data.id, type_id: 1})
                
                res.json({data: listDevis})
            }

        })
    },

    listFacture : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On récupère la liste des facture
                let listFacture = await db
                    .select('*')
                    .from('document')
                    .where({user_id: data.id, type_id: 2})
                
                res.json({data: listFacture})
            }
        })
    },

    listDocument : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let siteID = req.params.siteID;

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            console.log(data)
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On récupère la liste des devis
                let listDocument = await db
                    .select('*')
                    .from('document')
                    .where({user_id: data.id, site_id: siteID})
                
                res.json({data: listDocument})

            }
        })
    },

    findDocumentById : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let id = Number(req.params.idDocument);

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
 
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On récupère la liste des devis
                let document = await db
                    .select('document.id', 'number', 'client.id as client_id', 'client.contact_name as client', 'type.name as type', 'statut.name as statut', 'statut.id as statut_id', 'revision_number as revision', 'document.created_at', 'document.updated_at', 'document.site_id as site_id', 'site.name as site', 'client.compagny', 'client.adress', 'client.email', 'client.tel', 'client.logo')
                    .from('document')
                    .innerJoin('client', 'client.id', 'document.client_id')
                    .innerJoin('type', 'type.id', 'document.type_id')
                    .innerJoin('statut', 'statut.id', 'document.statut_id')
                    .innerJoin('site', 'site.id', 'document.site_id')
                    .where({'document.id': id})

                if(document.length === 0) {
                    res.json({error: "Le document que vous recherchez n'existe pas."})
                }
                
                res.json({data: document})
            }
        })
    },

    listAllDocument : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On récupère la liste des devis
                let listDocument = await db
                    .select('document.id', 'number', 'client.contact_name as client', 'type.name as type', 'statut.name as statut', 'revision_number as revision', 'document.created_at', 'document.updated_at', 'client.compagny')
                    .from('document')
                    .innerJoin('client', 'client.id', 'document.client_id')
                    .innerJoin('type', 'type.id', 'document.type_id')
                    .innerJoin('statut', 'statut.id', 'document.statut_id')
                    .where({user_id: data.id})
                
                res.json({data: listDocument})

            }
        })
    },
    
}

