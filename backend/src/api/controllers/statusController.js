// Connexion à la db
const db = require('../../config/db');
const jwt = require('jsonwebtoken');

require('dotenv').config({path:__dirname+'/config/.env'});

module.exports = {
    add : async(req, res) => {

        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');

        let name = req.body.name;
        let type_id = req.body.type;

        console.log(req.body)

        // On vérifie que le statut n'existe pas déjà
        let statusAlreadyExist = await db
                                .select('name')
                                .from('statut')
                                .where({name: name})
        if(statusAlreadyExist.length !== 0) {
            res.json({error: "Ce statut existe déjà. Veuillez en saisir un autre."})
        } else {
            jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
                if(err) {
                    res.json({error: err})
                } else {
                    await db('statut')
                        .insert({name: name, type_id: type_id})
                    res.json({data: "Ce statut a bien été ajouté"})
                }
            })
        }
    },

    delete : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let id = req.body.id;
        let origin = 1;

        // On verifie que le statut n'est pas présent dans un document avant de supprimer
        let statusFound = await db
            .select('document.statut_id', 'statut.origin', 'document.number')
            .from('document')
            .innerJoin('statut', 'statut.id', 'document.statut_id')
            .where({statut_id: id})

        if(statusFound.length !== 0) {
            res.json({error: "Ce statut est présent dans un document. Il ne peut être supprimé"})
        } else {
            // On verifie que le statut ne fait pas partie des statuts prédéfinis
            let statusOriginFound = await db
            .select('*')
            .from('statut')
            .where({origin: origin, id: id})

            if(statusOriginFound.length !== 0) {
                res.json({error: "Ce statut fait partie des statuts prédéfinis. Il ne peut être supprimé."})
            } else {
                jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
                    if(err) {
                        res.json({error: err})
                    } else {
                        await db('statut')
                            .where({id: id})
                            .del()                    
                        res.json({data: "Ce statut a bien été supprimé"})
                    }
                })
            }
        }
    },

    listStatus: async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            console.log(data)
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On récupère la liste des devis
                let listStatus= await db
                    .select('*')
                    .from('statut')
                
                res.json({data: listStatus})
            }

        })
    }
}