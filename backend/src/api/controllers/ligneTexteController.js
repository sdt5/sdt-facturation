const db = require('../../config/db');
const jwt = require('jsonwebtoken');

require('dotenv').config({path:__dirname+`/config/.env.${process.env.NODE_ENV}`});

module.exports = {
    add : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let ligneId = req.params.idLigne;
        let texte = req.body.texte;

        console.log(req.body)

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On vérifie qu'un texte n'existe pas déjà
                textAlreadyExist = await db
                                    .select('id')
                                    .from('ligne_texte')
                                    .where({ligne_id: ligneId})

                                    console.log(textAlreadyExist)

                if (textAlreadyExist.length !== 0) {
                    console.log('yoooo')
                    await db('ligne_texte')
                        .update({texte: texte})
                        .where({ligne_id: ligneId})
                } else {
                    console.log('yaaa')
                    await db('ligne_texte')
                    .insert({texte: texte, ligne_id: ligneId})
                    .where({ligne_id: ligneId})
                }                
                res.json({data: "Votre texte a bien été ajouté"})
            }

        })
    },
}