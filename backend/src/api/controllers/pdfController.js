const db = require('../../config/db');
const jwt = require('jsonwebtoken');
const dayjs = require('dayjs');
const Printer = require('pdfmake');
const path = require('path');
const fs = require('fs');
const os = require('os');
// const sendmail = require('sendmail')();
const sgMail = require('@sendgrid/mail');

require('dotenv').config({path:__dirname+`/config/.env.${process.env.NODE_ENV}`});


module.exports = {
    savePDF : async(req, res, next) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let idDocument = req.params.idDocument

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                let document = await db
                    .select(
                        'document.id', 'number', 'client.contact_name as client', 'type.name as type', 'statut.name as statut', 'revision_number as revision', 'document.created_at', 'document.updated_at', 'client.compagny', 'client.tel', 'client.adress')
                    .from('document')
                    .innerJoin('client', 'client.id', 'document.client_id')
                    .innerJoin('type', 'type.id', 'document.type_id')
                    .innerJoin('statut', 'statut.id', 'document.statut_id')
                    .where({'document.id': idDocument})
                
                let ligne = await db
                        .select('ligne.*', 'ligne_texte.texte', 'tva.taux')
                        .from('ligne')
                        .innerJoin('ligne_texte', 'ligne_texte.ligne_id', 'ligne.id')
                        .innerJoin('tva', 'tva.id', 'ligne.tva_id')
                        .where({document_id: idDocument})


                let addTable = ligne.map((item, i) => [i+1,`${item.pn_name}-${item.pn_description}`, item.texte, item.taux, item.quantity, item.prix_ht, item.total_ht_brut, item.prix_remise, item.prix_ttc])

                let tableHeader = ['Ligne', 'Libellé', 'Notes', 'TVA', 'Quantité', 'Prix unitaire HT', 'Prix HT', 'Prix HT après remise', 'Prix TTC']

                let tab = [tableHeader, ...addTable];

                let thtbrut = ligne.map((item) => item.total_ht_brut).reduce((a,b) => a+b, 0);
                let tr = ligne.map((item) => item.prix_remise).reduce((a,b) => a+b, 0);
                let ttc = ligne.map((item) => item.prix_ttc).reduce((a,b) => a+b, 0);
                        

                var docDefinition = {
                    content: [

                        {text: document[0].number},
                        {text: `Edité par : `},
                        {text: `SkyDevTeam`, style: 'subheader'},
                        {text: `109 Place Charles de Gaulle`},
                        {text: `74300 Cluses`},
                        {text: `skydevteam@adsoftware.fr`},
                        {text: `04 50 89 48 50`},
            
                        {text: `Pour le client : `},
                        {text: document[0].compagny, style: 'subheader'},
                        {text: `Contact : ${document[0].client}`},
                        {text: document[0].adress},
                        {text: document[0].tel},
                        {text: document[0].email},
                        {
                            style: 'tableExample',
                            table: {
                                headerRows: 1,
                                widths: [ 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto' ],
                        
                                body: tab
                            }
                        },
                        {
                            style: 'tableExample',
                            table: {
                              headerRows: 1,
                              widths: [ 'auto', 'auto', 'auto' ],
                              body: [
                                    ['Total HT brut', 'Total après remise', 'Total TTC'],
                                    [thtbrut, tr, ttc]
                              ]
                            }
                          }
                    ]
                };

                var printer = new Printer({
                    Roboto: {
                        normal: path.resolve('src', 'fonts', 'Roboto-Regular.ttf'),
                        bold: path.resolve('src', 'fonts', 'Roboto-Bold.ttf'),
                    }
                })
                var doc = printer.createPdfKitDocument(docDefinition)
                var writeStream = doc.pipe(fs.createWriteStream(`tmp/${document[0].number}.pdf`));
                
                doc.end()

                writeStream.on('finish', () => {
                    res.download(path.join(__dirname,`../../../tmp/${document[0].number}.pdf`))
                })
            }
        })
    },

    sendPdf : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let idDocument = req.params.idDocument;

        let document = await db
                    .select(
                        'document.id', 'number', 'client.contact_name as client', 'type.name as type', 'statut.name as statut', 'revision_number as revision', 'document.created_at', 'document.updated_at', 'client.compagny', 'client.tel', 'client.adress', 'client.email as client_email', 'user.email as user_email', 'user.name as user_name')
                    .from('document')
                    .innerJoin('client', 'client.id', 'document.client_id')
                    .innerJoin('user', 'user.id', 'document.user_id')
                    .innerJoin('type', 'type.id', 'document.type_id')
                    .innerJoin('statut', 'statut.id', 'document.statut_id')
                    .where({'document.id': idDocument})

        const pathToAttachment = path.join(__dirname,`../../../tmp/${document[0].number}.pdf`);
        const attachment = fs.readFileSync(pathToAttachment).toString("base64");

        const sgMail = require('@sendgrid/mail')
        sgMail.setApiKey(process.env.SENDGRID_API_KEY)
        const msg = {
        to: document[0].client_email, // Change to your recipient
        cc: document[0].user_email,
        from: 'portfolio@oceane-faula.com', // Change to your verified sender
        subject: document[0].number,
        text: 'and easy to do anywhere, even with Node.js',
        attachments: [
            {
              content: attachment,
              filename: "attachment.pdf",
              type: "application/pdf",
              disposition: "attachment"
            }
          ],
        html: `<p>Bonjour ${document[0].client}, Veuillez trouver en pj votre ${document[0].type}. <br> N'hésitez pas à répondre directement à ce mail pour plus d'information<p><strong>and easy to do anywhere, even with Node.js</strong>`,
        }
        sgMail
        .send(msg)
        .then(() => {
            console.log('Email sent');
            res.json({data:'Votre email a bien été envoyé au client. Une copie vous a également été transmise.'})
        })
        .catch((error) => {
            console.error(error);
            res.json({error: 'Votre email n\'a pas pu être envoyé'})
        })
    }

}