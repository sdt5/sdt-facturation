// Connexion à la db
const db = require('../../config/db');
const jwt = require('jsonwebtoken');

require('dotenv').config({path:__dirname+'/config/.env'});

module.exports = {
    add : async(req, res) => {

        let name = req.body.name;
        let code = req.body.name.toUpperCase();
        let increment = 0

        // On vérifie que le site n'existe pas déjà
        let siteAlreadyExist = await db
                                .select('name')
                                .from('site')
                                .where({name: name})
        if(siteAlreadyExist.length !== 0) {
            res.json({error: "Ce site existe déjà. Veuillez en saisir un autre."})
        } else {
            await db('site')
            .insert({name: name, code: code, increment: increment})

        
            res.json({data: "Votre site a bien été ajouté"})
        }
    },

    delete : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let id = req.body.id;

        // On verifie que le site n'est pas présent dans un document avant de supprimer
        let siteFound = await db
            .select('*')
            .from('document')
            .where({site_id: id})

        if(siteFound.length !== 0) {
            res.json({error: "Ce site est présent dans un document, il ne peut être supprimer"})
        } else {
            jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
                if(err) {
                    res.json({error: err})
                } else {
                    await db('site')
                        .where({id: id})
                        .del()                    
                    res.json({data: "Ce site a bien été supprimée"})
                }
            })
        }
    },

    listSite : async(req, res) => {

        let listSite = await db
            .select('*')
            .from('site')
        
        res.json({data: listSite})
    },

    findSiteById : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let id = Number(req.params.idSite);

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
 
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On récupère le site selon l'id
                let document = await db
                    .select('*')
                    .from('site')
                    .where({'site.id': id})

                if(document.length === 0) {
                    res.json({error: "Le document que vous recherchez n'existe pas."})
                }
                
                res.json({data: document})
            }
        })
    }

}