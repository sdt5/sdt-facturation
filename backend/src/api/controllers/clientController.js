// Connexion à la db
const db = require('../../config/db');
const jwt = require('jsonwebtoken');

require('dotenv').config({path:__dirname+'/config/.env'});

module.exports = {
    add : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        
        let compagny_input = (req.body.compagny);
        let contact_name_input = req.body.contact_name;
        let email_input = req.body.email;
        let tel_input = Number(req.body.tel);
        let adress_input = req.body.adress;

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, datajwt) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } 

            // Validation 
            else if(isNaN(tel_input)) {
                res.json({error: "Veuillez vérifier les données saisies"})
            } else {
                // On vérifie que ce client n'existe pas déjà
                let clientAlreadyExist = await db
                    .select('*')
                    .from('client')
                    .where({compagny: compagny_input})
                    .orWhere({email: email_input})
                
                console.log(clientAlreadyExist.length)
                if (clientAlreadyExist.length !== 0) {
                    res.json({error: "Ce client existe déjà"})
                } else {
                    // On ajoute le nouveau client
                    let { data } = await db('client')
                        .insert({compagny: compagny_input, contact_name: contact_name_input, email: email_input, tel: tel_input, adress: adress_input})
                    
                    res.json({data})
                }
            }
        })
    },

    update : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let idClient = req.body.id;
        let contact = req.body.contact;
        let tel = req.body.tel;
        let adress = req.body.adress;

        console.log(req.body)

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                await db('client')
                    .where({id: idClient})
                    .update({contact_name: contact})
                
                await db('client')
                    .where({id: idClient})
                    .update({tel: tel})
                
                await db('client')
                    .where({id: idClient})
                    .update({adress: adress})
                
                res.json({data: 'Les informations de ce client ont bien été modifiés'})
            }
        })
    },

    delete : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let idClient = req.body.clientID;

        // On verifie que le client n'est pas présent dans un document avant de supprimer
        let clientFound = await db
            .select('*')
            .from('document')
            .where({client_id: idClient})

        if(clientFound.length !== 0) {
            res.json({error: "Ce client est présent dans un document, il ne peut être supprimer"})
        } else {
            jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
                if(err) {
                    res.json({error: "Vous n'avez pas accès à ces ressources"})
                } else {
                    let client = await db('client')
                        .where({id: idClient})
                        .del()
                    
                    res.json({data: client})
                }
            })
        }
    },

    listClient: async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On récupère la liste des devis
                let listClient= await db
                    .select('*')
                    .from('client')
                
                res.json({data: listClient})
            }

        })
    },

    listClientBySite: async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let siteID = req.params.idSite

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On récupère la liste des devis
                let listClient= await db
                    .select('*')
                    .from('client')
                    .where({site_id: siteID})
                
                res.json({data: listClient})
            }

        })
    },

    findById : async(req, res) => {
        let headerAuth = req.headers.authorization;
        let token = headerAuth.replace('Bearer ', '');
        let idClient = req.params.idClient;

        jwt.verify(token, process.env.JWT_SIGN_SECRET, async(err, data) => {
            if(err) {
                res.json({error: "Vous n'avez pas accès à ces ressources"})
            } else {
                // On find l'utilisateur
                console.log(req.params.userId)
                let client = await db
                    .select('*')
                    .from('client')
                    .where({id: idClient})
                
                if (client) {
                    res.json({data: client})
                } else {
                    res.json({error: "Ce client n'existe pas"})
                }
                
                
            }
        })
    },
}