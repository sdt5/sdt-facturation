const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

// Express
const app = express();

// Mise en place des variables d'environnement
require('dotenv').config({path:`${__dirname}/config/.env.${process.env.NODE_ENV}`});

// Middleware pour avoir des info sur la route
app.use(morgan("dev"));

app.use(express.urlencoded({ extended: true }));

const environment = process.env.NODE_ENV || 'development';

// Middleware pour le traitement cors
app.use(cors());

const PORT = process.env.PORT ||  3001;

const router = require('./api/router');
const rootAPI = process.env.ROOT_API;


// Connexion à la db
const db = require('./config/db');

app.use(rootAPI, router);

// On exécute le front en static depuis l'api
const path = require('path');
app.use(express.static(path.join(__dirname, '../../frontend/build')));

// Création d'une route par défaut si la route n'existe pas
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../../frontend/build/index.html'));
  });

app.listen(PORT, () => console.log(`started on port ${PORT}`))

