
import axios from 'axios';
import qs from 'qs';
import { useMutation, useQuery, useQueryClient } from "react-query";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// ------------------------ DOCUMENT -----------------------------
export async function findDocumentById(idDocument, token) {
    return await axios.get(`/api/v1/document/${idDocument}`, {
        headers: {
            'Authorization' : `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        }
    })
}

export const useDocuments = (siteID, token) => {
    return useQuery('documents', () => {
        return axios.get(`/api/v1/user/login/list/document/${siteID}`, {
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then((res) => res.data)
    })
}

export const useDocument = (id, token) => {
    return useQuery('document', () => {
        return axios.get(`/api/v1/document/${id}`, {
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then((res) => res.data)
    })
}

// export const useSavePdfDocument = (id, token) => {
//     return useQuery('pdf', () => {
//         return axios.get(`/api/v1/document/${id}/pdf`, {
//             headers: {
//                 'Authorization' : `Bearer ${token}`,
//                 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
//             }
//         })
//     })
// }

// ---------------------------------------------------------------

// ------------------------ LIGNES -----------------------------
export async function findLigneByDocumentId(idDocument, token) {
    return await axios.get(`/api/v1/ligne/${idDocument}`, {
        headers: {
            'Authorization' : `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        }
    })
}

export const useLignesByDocumentId = (idDocument, token) => {
    return useQuery('lignes', () => {
        return axios.get(`/api/v1/ligne/${idDocument}`, {
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then((res) => res.data)
    }) 
}

export const useLigne = (idLigne, token) => {
    return useQuery('ligne', () => {
        return axios.get(`/api/v1/ligne/${idLigne}`, {
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then((res) => res.data)
    }) 
}

export const useAddLigneMutation = () => {
    const queryClient = useQueryClient();
    return useMutation(async(data) => {
        return await axios.post(`/api/v1/ligne/${data.idDocument}/add`, qs.stringify(data.dataLigne), {
            headers: {
                'Authorization' : `Bearer ${data.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
    },
    {
        onSuccess: (e) => {
            if(e.data.data) {
                queryClient.invalidateQueries('lignes')
                toast(`La ligne a bien été ajoutée à votre document`, { type: "success" });
            } else {
                toast(e.data.error, { type: "error" });
            }
        },
        onError: (err) => {
            toast(`error ${err.message}`, { type: "error" });
        },
    })
}

export const useDeleteLigneMutation = () => {
    const queryClient = useQueryClient();
    return useMutation(async(data) => {
        return await axios.post(`/api/v1/ligne/${data.idDocument}/delete`, qs.stringify(data.idLigne), {
            headers: {
                'Authorization' : `Bearer ${data.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
    },
    {
        onSuccess: (e) => {
            if(e.data.data) {
                queryClient.invalidateQueries('lignes')
                toast(`La ligne a bien été supprimée de votre document`, { type: "success" });
            } else {
                toast(e.data.error, { type: "error" });
            }
        },
        onError: (err) => {
            toast(`error ${err.message}`, { type: "error" });
        },
    })
}

export const useUpdateLigneMutation = () => {
    const queryClient = useQueryClient();
    return useMutation(async(data) => {
        return await axios.post(`/api/v1/ligne/${data.idLigne}/update`, qs.stringify(data), {
            headers: {
                'Authorization' : `Bearer ${data.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
    },
    {
        onSuccess: (e) => {
            if(e.data.data) {
                queryClient.invalidateQueries('lignes')
                toast(`Cette ligne a bien été modifiée`, { type: "success" });
            } else {
                toast(e.data.error, { type: "error" });
            }
        },
        onError: (err) => {
            toast(`error ${err.message}`, { type: "error" });
        },
    })
}
// ---------------------------------------------------------------


// ------------------------ USER -----------------------------
export async function listUserFunc(token) {
    return await axios.get('/api/v1/list/user', {
        headers: {
            'Authorization' : `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        }
    })
}

export async function findUserFunc(idUtilisateur,token) {
    return await axios.get(`/api/v1/user/${idUtilisateur}`, {
        headers: {
            'Authorization' : `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        }
    })
}

export const useUsers = (token) => {
    return useQuery('users', () => { 
        return axios.get(`/api/v1/list/user`, {
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then((res) => res.data)
    })
}

export const useUser = (idUtilisateur,token) => {
    return useQuery('user', () => { 
        return axios.get(`/api/v1/user/${idUtilisateur}`, {
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then((res) => res.data)
    })
}

export const useDeleteUserMutation = () => {
    const queryClient = useQueryClient();
    return useMutation(async(data) => { 
        return await axios.post("/api/v1/user/delete", qs.stringify(data), {
            headers: {
                'Authorization' : `Bearer ${data.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
    },
    {
        onSuccess: (e) => {
            if(e.data.data) {
                queryClient.invalidateQueries("users");
                toast(e.data.data, { type: "success" });
            } else {
                toast(e.data.error, { type: "error" });
            }
        },
        onError: (err) => {
            toast(`error ${err.message}`, { type: "error" });
        },
    }
    )
};

export async function deleteUser(userID, token) {
    let idUser = {
        id_user: userID
    }
    return await axios.post(`/api/v1/user/delete`, qs.stringify(idUser), {
        headers: {
            'Authorization' : `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        }
    })
}
// ---------------------------------------------------------------


// ------------------------ SITE -----------------------------
export async function listSites(token) {
    return await axios.get('/api/v1/list/site', {
        headers: {
            'Authorization' : `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        }
    })
}

export const useSites =  (token) => {
    return useQuery('sites', () => {
        return axios.get('/api/v1/list/site', {
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then((res) => res.data)
    }) 
}

export const useSite =  (id, token) => {
    return useQuery('site', () => {
        return axios.get(`/api/v1/site/${id}`, {
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then((res) => res.data)
    }) 
}

export const useCreateSiteMutation = () => {
    return useMutation(async(data, token) => { 
        return await axios.post("/api/v1/site/add", qs.stringify(data), {
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
    },
    {
        onSuccess: (e) => {
            if(e.data.data) {
                toast(`Le site a bien été créé`, { type: "success" });
            } else {
                toast(e.data.error, { type: "error" });
            }
        },
        onError: (err) => {
            toast(`error ${err.message}`, { type: "error" });
        },
    }
    )
};

export const useDeleteSiteMutation = () => {
    const queryClient = useQueryClient();
    return useMutation(async(data) => { 
        return await axios.post("/api/v1/site/delete", qs.stringify({id: data.id}), {
            headers: {
                'Authorization' : `Bearer ${data.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
    },
    {
        onSuccess: (e) => {
            if(e.data.data) {
                queryClient.invalidateQueries("sites");
                toast(`Le site a bien été supprimé`, { type: "success" });
            } else {
                toast(e.data.error, { type: "error" });
            }
        },
        onError: (err) => {
            toast(`error ${err.message}`, { type: "error" });
        },
    }
    )
};
// ---------------------------------------------------------------

// ------------------------ CLIENT -----------------------------
export const useClients =  (token) => {
    return useQuery('clients', () => {
        return axios.get('/api/v1/list/client', {
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then((res) => res.data)
    }) 
}

export const useClient =  (idClient, token) => {
    return useQuery('client', () => {
        return axios.get(`/api/v1/client/${idClient}`, {
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then((res) => res.data)
    }) 
}

export const useDeleteClientMutation = () => {
    const queryClient = useQueryClient();
    return useMutation(async(data) => { 
        return await axios.post("/api/v1/client/delete", qs.stringify(data), {
            headers: {
                'Authorization' : `Bearer ${data.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
    },
    {
        onSuccess: (e) => {
            if(e.data.data) {
                queryClient.invalidateQueries("clients");
                toast(`Le client a bien été supprimé`, { type: "success" });
            } else {
                toast(e.data.error, { type: "error" });
            }
        },
        onError: (err) => {
            toast(`error ${err.message}`, { type: "error" });
        },
    }
    )
};

export const useUpdateClientMutation = () => {
    const queryClient = useQueryClient();
    return useMutation(async(data) => { 
        return await axios.post("/api/v1/client/update", qs.stringify(data), {
            headers: {
                'Authorization' : `Bearer ${data.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
    },
    {
        onSuccess: (e) => {
            if(e.data.data) {
                queryClient.invalidateQueries("client");
                toast(e.data.data, { type: "success" });
            } else {
                toast(e.data.error, { type: "error" });
            }
        },
        onError: (err) => {
            toast(`error ${err.message}`, { type: "error" });
        },
    }
    )
};

// ---------------------------------------------------------------

// ------------------------ TVA -----------------------------
export async function listTVAFunc(token) {
    return await axios.get('/api/v1/list/tva', {
        headers: {
            'Authorization' : `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        }
    })
}

export const useAddTvaMutation = () => {
    const queryClient = useQueryClient();
    return useMutation(async(data) => {
        return await axios.post(`/api/v1/tva/add`, qs.stringify(data), {
            headers: {
                'Authorization' : `Bearer ${data.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
    },
    {
        onSuccess: (e) => {
            if(e.data.data) {
                queryClient.invalidateQueries('tvas')
                toast(e.data.data, { type: "success" });
            } else {
                toast(e.data.error, { type: "error" });
            }
        },
        onError: (err) => {
            toast(`error ${err.message}`, { type: "error" });
        },
    })
}

export const useDeleteTvaMutation = () => {
    const queryClient = useQueryClient();
    return useMutation(async(data) => { 
        return await axios.post("/api/v1/tva/delete", qs.stringify(data), {
            headers: {
                'Authorization' : `Bearer ${data.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
    },
    {
        onSuccess: (e) => {
            if(e.data.data) {
                queryClient.invalidateQueries("tvas");
                toast(`Cette TVA a bien été supprimée`, { type: "success" });
            } else {
                toast(e.data.error, { type: "error" });
            }
        },
        onError: (err) => {
            toast(`error ${err.message}`, { type: "error" });
        },
    }
    )
};

export const useTVAS =  (token) => {
    return useQuery('tvas', () => {
        return axios.get('/api/v1/list/tva', {
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then((res) => res.data)
    }) 
}
// ---------------------------------------------------------------


// ------------------------ STATUTS -----------------------------
export const useStatuts =  (token) => {
    return useQuery('statuts', () => {
        return axios.get('/api/v1/list/status', {
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then((res) => res.data)
    }) 
}

export const useDeleteStatutMutation = () => {
    const queryClient = useQueryClient();
    return useMutation(async(data) => { 
        return await axios.post("/api/v1/status/delete", qs.stringify({id: data.id}), {
            headers: {
                'Authorization' : `Bearer ${data.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
    },
    {
        onSuccess: (e) => {
            if(e.data.data) {
                queryClient.invalidateQueries("statuts");
                toast(`Le statut a bien été supprimé`, { type: "success" });
            } else {
                toast(e.data.error, { type: "error" });
            }
        },
        onError: (err) => {
            toast(`error ${err.message}`, { type: "error" });
        },
    }
    )
};

export async function listStatusFunc(token) {
    return await axios.get('/api/v1/list/status', {
        headers: {
            'Authorization' : `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        }
    })
}

export const useCreateStatusMutation = () => {
    return useMutation(async(data, token) => { 
        return await axios.post("/api/v1/status/add", qs.stringify({name: data.name, type: data.type}), {
            headers: {
                'Authorization' : `Bearer ${data.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
    },
    {
        onSuccess: (e) => {
            if(e.data.data) {
                toast(`Le statut a bien été créé`, { type: "success" });
            } else {
                toast(e.data.error, { type: "error" });
            }
        },
        onError: (err) => {
            toast(`error ${err.message}`, { type: "error" });
        },
    }
    )
};
// ---------------------------------------------------------------



// ------------------------ SITES -----------------------------
export async function findSiteById(idSite, token) {
    return await axios.get(`/api/v1/site/${idSite}`, {
        headers: {
            'Authorization' : `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        }
    })
}

export async function findSites(token) {
    return await axios.get(`/api/v1/list/site`, {
        headers: {
            'Authorization' : `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        }
    })
}
// ---------------------------------------------------------------


// ------------------------ EC -----------------------------
export async function findECByIdSite(idSite, token) {
    return await axios.get(`/api/v1/ec/${idSite}`, {
        headers: {
            'Authorization' : `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        }
    })
}


// ------------------------ PN -----------------------------
export async function findPN(keyword) {
    return await axios.get(`https://app-67fd1eb3-90a0-421e-a58b-b56a3ce596c1.cleverapps.io/api/parts/search?page=1&keyword=${keyword}`, {
        headers: {
            'Authorization' : `Bearer abcdef`,
            'Content-Type': 'application/json',
        }
    })
}

export async function findPNById(id) {
    return await axios.post(`https://app-67fd1eb3-90a0-421e-a58b-b56a3ce596c1.cleverapps.io/api/parts`, 
    {
        "ids": [id]
    }, 
    {
        headers: {
            'Authorization' : `Bearer abcdef`,
            'Content-Type': 'application/json',
        }
    })
}






