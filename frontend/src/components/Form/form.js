import { useStore } from '../../store/store';
import { useState } from 'react'
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import qs from 'qs';
import { Button, TextField, Select, MenuItem, InputLabel, FormControl, InputAdornment } from '@material-ui/core';
import { AccountCircle } from '@material-ui/icons';
import { useSites } from '../../functions/functions';
import { toast, ToastContainer } from 'react-toastify';
import './style.css';

const Form = () => {

    const {state, dispatch} = useStore();

    const [site, setSite] = useState("");

    const { data: sites, status: statusSites } = useSites(state.token);

    if(state.connected) {
        return <Redirect to="/profil" />
    }

    const handleChange = (e) => {
        setSite(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        let name = e.target.name.value;
        let pin = e.target.pin.value;
        let siteID = e.target.site.value

        // On va récupérer un JWT en fournissant nos ID dans le body de la requête POST
        axios.post(`/api/v1/user/login/`, qs.stringify({
            name: name,
            pin: pin,
            site: siteID
        }))
        .then(async(res) => {
            if(res.data.error) {
                toast(res.data.error, { type: "error" });
            } else {
                let token = res.data.token

                // On stock le token dans le local storage
                localStorage.setItem('token', token)

                // On stock le status de connexion dans le local storage
                localStorage.setItem('connected', true)

                // On stock l'id du site dans le local storage
                localStorage.setItem('siteID', siteID)

                // On stock le token dans le store
                dispatch({type: 'TOKEN', token: localStorage.token})

                // On stock le status de connexion le store
                dispatch({type: 'CONNECTED', connected: localStorage.connected})

                // On stock l'id du site le store
                dispatch({type: 'SITE', siteID: localStorage.siteID})

                // On accède à la requête get en fournissant le token dans le header
                await axios.get(`/api/v1/user/login/`, {
                    headers: {
                        'Authorization' : `Bearer ${token}`,
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
                    }
                })
            }
        })
    }

    return(
        <form className="form_auth margin_form_auth" autoComplete="off" onSubmit={handleSubmit}>
            <ToastContainer />
            <TextField 
                id="outlined-basic" 
                label="Nom" 
                defaultValue="sdt"
                InputProps={{
                    startAdornment: (
                        <InputAdornment position="start">
                        <AccountCircle />
                        </InputAdornment>
                    ),
                }} 
                variant="outlined" 
                name="name"
                required
                margin="dense" />
            <TextField 
                id="outlined-basic" 
                label="Code PIN" 
                defaultValue="1234"
                InputProps={{
                    startAdornment: (
                        <InputAdornment position="start" />
                    ),
                }} 
                variant="outlined" 
                required 
                name="pin" 
                margin="dense"/>

            <FormControl 
                variant="outlined" 
                required 
                margin='dense'
                label="Veuillez selectionner un site pour vous connecter">
                <InputLabel id="demo-simple-select-outlined-label" variant="outlined" >Veuillez selectionner un site pour vous connecter</InputLabel>
                <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    onChange={handleChange}
                    name="site"
                    value={site}
                    label="Veuillez selectionner un site pour vous connecter"
                    >
                    {statusSites === 'success' &&
                        sites.data.map((site) => {
                            return(
                                <MenuItem key={site.id} value={site.id}>{site.name}</MenuItem>
                            )
                        })
                    }
                </Select>
            </FormControl>
            <Button type="submit" variant="contained" color="primary" margin="dense">Se connecter</Button>
        </form>
    )
}

export default Form;