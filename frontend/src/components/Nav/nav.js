import { useStore } from '../../store/store';
import { Link } from 'react-router-dom';
import './style.css';

const Nav = () => {

    // On récurère notre globalState
    const { dispatch } = useStore();

    const handleLogout = () => {

        // On supprime les data contenu dans le local storage (token + statut de connexion)
        // On modifie notre état de connexion dans le state
        localStorage.clear();
        dispatch({type: 'CONNECTED', connected: false})
    }

    return(
        <nav>
            <ul>
                <li className="nav_item flex"><Link to="/profil" className="align_center">Mes documents</Link></li>
                <li className="nav_item flex"><Link to="/clients" className="align_center">Clients</Link></li>
                <li className="nav_item flex"><Link to="/utilisateurs" className="align_center">Utilisateurs</Link></li>
                <li className="nav_item flex"><Link to="/sites" className="align_center">Sites</Link></li>
                <li className="nav_item flex"><Link to="/statuts" className="align_center">Statuts des documents</Link></li>
                <li className="nav_item flex"><Link to="/tva" className="align_center">TVA</Link></li>
                <li className="nav_item flex" onClick={handleLogout}><Link to="/" className="align_center">Se déconnecter</Link></li>
            </ul>
        </nav>
    )
}

export default Nav;