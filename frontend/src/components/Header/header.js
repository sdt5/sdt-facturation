import './style.css';
import { useStore } from '../../store/store';
import WelcomeSite from '../WelcomeSite/welcomeSite';

const Header = () => {

    // On récurère notre globalState
    const { state } = useStore({});

    return(
        <header>
            <h2 className="h2_header_logo">Projet SkyDevTeam</h2>
            {state.connected && <WelcomeSite />}
        </header>
    )
}

export default Header;