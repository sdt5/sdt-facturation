import { Link } from 'react-router-dom';

const Disconnect = () => {

    return(
        <div>
            <p>
                Vous avez été déconnectez, veuillez vous connecter à nouveau en cliquant sur {<Link to="/" >ce lien.</Link>}
            </p>
        </div>
    )
}

export default Disconnect;