import { useStore } from '../../store/store';
import { useSites, useSite, useUser } from '../../functions/functions';
import { useQueryClient } from "react-query";
import { Select, InputLabel, FormControl } from '@material-ui/core';
import jwt_decode from "jwt-decode";

const WelcomeSite = () => {

    // On récurère notre globalState
    const { state, dispatch } = useStore({});

    const userId = jwt_decode(state.token).id

    const querySites = useSites(state.token);
    const querySite = useSite(localStorage.siteID, state.token);
    const queryUser = useUser(userId, state.token)

    const queryClient = useQueryClient();

    const handleChangeSelect = async (e) => {

        await localStorage.setItem('siteID', e.target.value)
        // On stock l'id du site dans le store
        await dispatch({type: 'SITE', siteID: localStorage.siteID})

        await queryClient.removeQueries('site')
        await queryClient.removeQueries('documents')
        await querySite.refetch()
        await querySite.refetch()
        .then((res) => console.log(res))
    }

    return (
        <>
        {queryUser.status === 'success' && <p className="align_center">Bonjour, {queryUser.data.data[0].name} </p>}
            <div className="flex">
                {querySites.status === 'success' && 
                    <FormControl variant="filled">
                        <InputLabel shrink htmlFor="site" color='secondary'>
                            Site
                        </InputLabel>
                        <Select
                        native
                        name="site"
                        value={state.siteID}
                        onChange={handleChangeSelect}
                        inputProps={{
                            name: 'site',
                            id: 'site',
                        }}>
                            {querySites.data.data.map((site, i) => {return <option key={i} value={site.id}>{site.name}</option>})}
                        </Select>
                  </FormControl>

                }
            </div>
        </>
    )
}

export default WelcomeSite;