import './style.css';

const Footer = () => {

    return(
        <footer>
            <p>
                © Site réalisé par SkyDevTeam <br />
                All rights reserved.
            </p>
        </footer>
    )
}

export default Footer;