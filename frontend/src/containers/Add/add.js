import Header from "../../components/Header/header";
import Nav from "../../components/Nav/nav";
import Footer from "../../components/Footer/footer";
import { Link } from 'react-router-dom';

const Add = (message, data) => {

console.log(message)
    return(
        <div>
            <Header />
            <main className="main_page">
                <Nav />
                <p>Votre document a bien été créé.<br/>
                <Link to="/profil">Vous pouvez retourner à la page "Mes documents".</Link></p>
            </main> 
            <Footer />
        </div>
    )
}

export default Add;