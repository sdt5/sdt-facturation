import Nav from "../../components/Nav/nav";
import { useStore } from '../../store/store';
import Header from "../../components/Header/header";
import Footer from "../../components/Footer/footer";
import Disconnect from "../../components/Disconnect/disconnect";
import { Link } from 'react-router-dom';
import { useSites, useDeleteSiteMutation } from '../../functions/functions';
import { ToastContainer } from 'react-toastify';
import { DataGrid } from '@material-ui/data-grid';
import { Button } from '@material-ui/core';

const Sites = () => {

    // On récurère notre globalState
    const { state } = useStore();

    const { data, status } = useSites();
    const deleteSiteMutation = useDeleteSiteMutation();

    const handleClicDeleteSite = (e) => {
        deleteSiteMutation.mutate({id: e.value, token: state.token});
    }

    const columns = [
        { field: 'id', headerName: 'ID', flex: 0.3, hide: true},
        { field: 'name', headerName: 'Nom', flex: 1, editable: false },
        { field: 'code', headerName: 'Code', flex: 1, editable: false },
        {
            field: 'action',
            headerName: 'Action',
            renderCell: (e) => (
                <Button variant="contained" color="secondary" onClick={() => {handleClicDeleteSite(e)}}>
                    Supprimer
                </Button>
            ),
            flex: 0.5
        },
    ]

    const rows = status === 'success' ? data.data.map((item, i) => {
        return(
            {
                id: i+1,
                name: item.name,
                code: item.code,
                action: item.id
            }
        )
    }) : [];

    return(
        <div>
            <Header />
            {!state.connected && 
                <Disconnect />
            }
            {state.connected && 
            
                <div>
                    <main className="main_page">
                        <Nav />
                        <ToastContainer />
                        <div className="table_side">
                            <div style={{ width: '95%', margin: '2rem' }}>
                                <Button variant="contained" color="primary"><Link to="/ajout/site">Créer un nouveau site</Link></Button>
                                <DataGrid rows={rows} columns={columns} autoHeight={true}  pageSize={5}/>  
                            </div>
                        </div>
                    </main>
                </div>
            }
            <Footer />
        </div>
    )
}

export default Sites;