import Header from "../../components/Header/header";
import Footer from "../../components/Footer/footer";
import './style.css';
import { useStore } from '../../store/store';
import { useParams } from 'react-router-dom';
import Disconnect from "../../components/Disconnect/disconnect";
import Nav from "../../components/Nav/nav";
import { 
    useClient,
    useUpdateClientMutation
} from '../../functions/functions';
import { ToastContainer } from 'react-toastify';
import { Button, TextField, InputAdornment } from '@material-ui/core';

const Client = () => {

    // On récurère notre globalState
    const { state } = useStore();


    let { idClient } = useParams();

    const { data: clientRQ, status: statusClient } = useClient(idClient, state.token);
    const updateClientMutation = useUpdateClientMutation();

    const handleSubmit = (e) => {
        e.preventDefault()
        let paramsQR = {
            token: state.token,
            id: idClient,
            contact: e.target.contact.value,
            tel: e.target.tel.value,
            adress: e.target.adress.value
        }
        updateClientMutation.mutate(paramsQR)
    }


    const tableClient = (document) => {
        return(
            <div>
            <form className="form_client" onSubmit={handleSubmit}>
                <h2>{document.compagny}</h2>
                <TextField 
                id="outlined-basic" 
                label="Contact" 
                defaultValue={document.contact_name}
                InputProps={{
                    startAdornment: (
                        <InputAdornment position="start" />
                    ),
                }} 
                variant="outlined" 
                required 
                name="contact" 
                register
                margin="dense"/>

                <TextField 
                id="outlined-basic" 
                label="Téléphone" 
                defaultValue={document.tel}
                InputProps={{
                    startAdornment: (
                        <InputAdornment position="start" />
                    ),
                }} 
                variant="outlined" 
                required 
                name="tel" 
                margin="dense"/>

                <TextField 
                id="outlined-basic" 
                label="Adresse" 
                defaultValue={document.adress}
                InputProps={{
                    startAdornment: (
                        <InputAdornment position="start" />
                    ),
                }} 
                variant="outlined" 
                required 
                name="adress" 
                margin="dense"/>

                <Button type="submit"  variant="contained" color="primary">MODIFIER LES INFORMATIONS</Button>
            </form>
            </div>
        )
    }

    return(
        <div>
            <Header />
            {!state.connected &&
                <Disconnect />
            }
            {state.connected &&
                <main className="main_page">
                    <Nav />
                    <ToastContainer />
                    {statusClient === "loading" && <div>Loading...</div>}
                    {statusClient === "error" && <div>Error fetching client</div>}
                    {statusClient === "success" && <div className="table_client">{tableClient(clientRQ.data[0])}</div>}
                </main>
            }
            <Footer />
        </div>
    )
}

export default Client;