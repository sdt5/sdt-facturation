import Header from "../../components/Header/header"
import Footer from "../../components/Footer/footer"
import { useStore } from '../../store/store';
import Disconnect from "../../components/Disconnect/disconnect";
import { useQuery } from 'react-query';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import Nav from "../../components/Nav/nav";
import jwt_decode from "jwt-decode";
import qs from 'qs';
import { useState } from 'react';
import './style.css';
import { DataGrid } from '@material-ui/data-grid';
import { toast, ToastContainer } from 'react-toastify';
import { TextField, Button, FormControl, InputLabel, Select, MenuItem, Card, CardContent, Typography } from '@material-ui/core';
import { 
    listStatusFunc, 
    listTVAFunc, 
    findPN, 
    useDocument, 
    useAddLigneMutation, 
    useLignesByDocumentId, 
    useDeleteLigneMutation, 
    useUpdateLigneMutation
} from '../../functions/functions';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import fileDownload from 'js-file-download';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;



const Document = () => {

    // On récurère notre globalState
    const { state } = useStore();

    const [items, setItems] = useState([]);

    let { idDocument } = useParams();

    // Fetch PN by name or description
    const fetchPN = async(e) => {
        let keyword = String(e.target.value);
        if (keyword.length >= 3) {
            findPN(keyword)
            .then((res) => setItems(res.data))
        }
    }

    const { data: document, status: statusDocument } = useDocument(idDocument, state.token);
    const { data: lignesRQ, status: statusLignes } = useLignesByDocumentId(idDocument, state.token);
    const queryStatuts = useQuery("statuts", () => listStatusFunc(state.token));
    const queryTVA = useQuery("tva", () => listTVAFunc(state.token));

    const addLigneMutation = useAddLigneMutation();
    const deleteLigneMutation = useDeleteLigneMutation();
    const updateLigneMutation = useUpdateLigneMutation();

    const handleChangeStatus = async(e) => {
        let tokenDecoded = jwt_decode(state.token)
        let updateDocumentData = {
            idStatut: e.target.value,
            idDocument: tokenDecoded.id
        }

        await axios.post(`/api/v1/document/update/${idDocument}`, qs.stringify(updateDocumentData), {
            headers: {
                'Authorization' : `Bearer ${state.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
        .then((res) => toast(`Le statut de votre document a bien été modifié`, { type: "success" }));
    }

    const handleAddligne = async(e) => {
        let dataItemID = {
            "ids": [e.id]
        }

        const { data } = await axios.post(`https://app-67fd1eb3-90a0-421e-a58b-b56a3ce596c1.cleverapps.io/api/parts`, dataItemID, {
                headers: {
                    'Authorization' : `Bearer abcdef`,
                    'Content-Type': 'application/json',
                }
            })

        let dataLigne = {
            pn_id: data.items[0].id,
            quantity: 1,
            prix_unitaire_ht: data.items[0].price,
            prix_ht: data.items[0].price,
            remise: 0,
            prix_ht_remise: data.items[0].price,
            tva: 1,
            prix_ttc: data.items[0].price * (1 + 20/100),
            pn_description: data.items[0].description,
            pn_name: data.items[0].name
        }

        let paramMutation = {
            idDocument: idDocument,
            dataLigne: dataLigne,
            token: state.token
        }

        setOpen(false);

        addLigneMutation.mutate(paramMutation)
    }

    const handleChangeNote = async(e, ligne) => {
        let data = {
            texte: e.target.value
        }

        await axios.post(`/api/v1/ligneTexte/add/${ligne.ligneId}`, qs.stringify(data), {
            headers: {
                'Authorization' : `Bearer ${state.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
            }
        })

    }

    const handleClicDeleteLigne = async(ligne) => {
        let paramsQR = {
            idDocument: idDocument,
            idLigne: {
                id_ligne: ligne
            },
            token: state.token
        }
        deleteLigneMutation.mutate(paramsQR)
    }

    const tableDocument = (document, statuts) => {
        return(
            <div>
                <form className="wrapper">
                    <div className="one">
                        <Card>
                            <CardContent>
                                <Typography color="textSecondary" gutterBottom>
                                    De
                                </Typography>
                                <Typography variant="h5" component="h2">
                                    SkyDevTeam
                                </Typography>
                                <Typography variant="body2" component="p">
                                    109 Place Charles de Gaulle
                                </Typography>
                                <Typography variant="body2" component="p">
                                    74300 Cluses
                                </Typography>
                                <Typography variant="body2" component="p">
                                    skydevteam@adsoftware.fr
                                </Typography>
                                <Typography variant="body2" component="p">
                                    04 50 89 48 50
                                </Typography>
                            </CardContent>
                        </Card>
                    </div>

                    <div className="one_2">
                        <h2>{document.number}</h2>
                        <Typography color="textSecondary" gutterBottom>
                            Réalisé le: {document.created_at}
                        </Typography>
                        <FormControl variant="filled">
                            <InputLabel id="demo-simple-select-filled-label">Statuts</InputLabel>
                            <Select
                            labelId="demo-simple-select-filled-label"
                            id="demo-simple-select-filled"
                            defaultValue={document.statut_id}
                            onChange={handleChangeStatus}
                            >
                            {statuts && statuts.data.map((statut, i) => {
                                return <MenuItem key={i} value={statut.id}>{statut.name}</MenuItem>
                            })
                            }
                            </Select>
                        </FormControl>
                    </div>

                    <div className="two">
                        <Card>
                            <CardContent>
                                <Typography color="textSecondary" gutterBottom>
                                    A
                                </Typography>
                                <Typography variant="h5" component="h2">
                                    {document.compagny}
                                </Typography>
                                <Typography color="textSecondary" gutterBottom>
                                    Contact: {document.client}
                                </Typography>
                                <Typography variant="body2" component="p">
                                    {document.adress}
                                </Typography>
                                <Typography variant="body2" component="p">
                                    {document.tel}
                                </Typography>
                                <Typography variant="body2" component="p">
                                    {document.email}
                                </Typography>
                            </CardContent>
                        </Card>
                    </div>
                </form>
            </div>
        )
    }

    const partNumber = () => {
        const columnsSearch = [
            {field: 'id', headerName: 'id', hide: true},
            {field: 'name', headerName: 'Nom du produit', flex: 1},
            {field: 'description', headerName: 'Description', flex: 1},
            {field: 'price', headerName: 'Prix', flex:0.5},
            {
                field: 'action',
                headerName: 'Action',
                renderCell: (e) => (
                    <Button variant="contained" color="secondary" onClick={() => {handleAddligne(e)}}>
                        Ajouter
                    </Button>
                ),
                flex: 1
            },
        ]

        const ligneGridSearch = items.items ? items.items.map((item, i) => {
            return(
                {
                    id: item.id,
                    name: item.name,
                    description: item.description,
                    price: item.price,
                    action: item.id
                }
            )
        }) : null;
        return(
            <div className="part_number five">
                <form className="center">
                    <label htmlFor="search">Rechercher un produit </label>
                    <input id="search" name="search" type="text" onChange={fetchPN} />
                </form>

                {items.items &&
                    <DataGrid rows={ligneGridSearch} columns={columnsSearch} pageSize={5} rowsPerPageOptions={[5, 10, 20]} autoHeight={true} rowHeight={50} />
                }
            </div>
        )
    }

    const columns = [ 
        { field: 'id', headerName: 'ID', flex: 0.3, hide: true},
        { field: 'ligne', headerName: 'Ligne', flex: 0.5, editable: false },
        { field: 'libelle', headerName: 'Libellé', flex: 1, editable: false },
        {
            field: 'notes',
            headerName: 'Notes',
            renderCell: (e) => (
                <TextField id="filled-basic" variant="filled" fullWidth={true} multiline={true} rows={5} defaultValue={e.row.note.note} onChange={(event) => {handleChangeNote(event, e.row.note)}}  />
            ),
            flex: 1.3
        },
        {
            field: 'tva',
            headerName: 'TVA',
            renderCell: (e) => (
                <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                name='tva'
                defaultValue={e.row.tva.tva_id ? e.row.tva.tva_id : null}
                onChange={(event) => {handleEditTVACellChange(event, e.id)}}
                label="Age"
                >
                    {queryTVA.status === 'success' && queryTVA.data.data.data.map((tv, i) => {
                        return <MenuItem key={i} value={tv.id}>{tv.taux}% - {tv.name}</MenuItem>
                    })}
                </Select>
            ),
            flex: 0.8,
            editable: true
        },
        { field: 'quantity', headerName: 'Quantité', flex: 0.5, type: 'number', editable: true },
        { field: 'prix_unitaire_ht_brut', headerName: 'Prix unitaire HT', flex: 0.7 },
        { field: 'prix_ht_brut', headerName: 'Prix HT brut', flex: 0.6 },
        { field: 'prix_ht_remise', headerName: 'Prix HT après remise', flex: 1 },
        { field: 'prix_ttc', headerName: 'Prix TTC', flex: 0.6 },
        {
            field: 'action',
            headerName: 'Action',
            renderCell: (e) => (
              <Button variant="contained" color="secondary" onClick={() => {handleClicDeleteLigne(e.id)}}>
                Supprimer
              </Button>
            ),
            flex: 1
        },
    ]

    const lignesGrid = statusLignes === 'success' ? lignesRQ.data.map((ligne, i) => {
        return( 
            {
                id: ligne.ligne_id,
                ligne: i+1,
                libelle: `${ligne.pn_name} - ${ligne.pn_description}`,
                note: {
                    ligneId:ligne.ligne_id,
                    note: ligne.texte
                },
                tva: ligne,
                quantity: ligne.quantity,
                prix_unitaire_ht_brut: ligne.prix_ht,
                prix_ht_brut: ligne.total_ht_brut,
                prix_ht_remise: ligne.prix_remise,
                prix_ttc: ligne.prix_ttc,
                action: ligne.ligne_id,
            }
        )
    }) : [];

    const rows = lignesGrid;

    const handleEditCellChange = (e) => {
        let paramsQR = {
            field: e.field,
            value: e.props.value,
            idLigne: e.id,
            token: state.token
        }
        updateLigneMutation.mutate(paramsQR)
    }
    const handleEditTVACellChange = (event, id) => {
        let paramsQR = {
            field: event.target.name,
            value: event.target.value,
            idLigne: id,
            token: state.token
        }
        updateLigneMutation.mutate(paramsQR)
    }

    const handleSavePdf = async() => {
        let doc = await axios.get(`/api/v1/document/${idDocument}/pdf`, {
            headers: {
                'Authorization' : `Bearer ${state.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            },
            responseType: 'blob'
        })
        fileDownload(doc.data, statusDocument === 'success' ? document.data[0].number : 'document.pdf');
    }

    const handleSendPdf = async() => {
        await axios.get(`/api/v1/document/${idDocument}/send`, {
            headers: {
                'Authorization' : `Bearer ${state.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
        .then((res) => {
            if (res.data.data) {
                toast(res.data.data, { type: "success" });
            }
            if (res.data.error) {
                toast(res.data.error, { type: "error" });
            }
        })
    }


    const columnsTotaux = [ 
        { field: 'total_ht_brut', headerName: 'Total HT brut', flex: 1},
        { field: 'total_remise', headerName: 'Total après remise', flex: 1, editable: false },
        { field: 'total_ttc', headerName: 'Total TTC', flex: 1, editable: false },
    ]

    let thtbrut = statusLignes === 'success' ? lignesRQ.data.map((ligne) => ligne.total_ht_brut).reduce((a,b) => a+b, 0) : 0;
    let tr = statusLignes === 'success' ? lignesRQ.data.map((ligne) => ligne.prix_remise).reduce((a,b) => a+b, 0) : 0;
    let ttc = statusLignes === 'success' ? lignesRQ.data.map((ligne) => ligne.prix_ttc).reduce((a,b) => a+b, 0) : 0;

    const lignesTotauxGrid = [
        {id: 1, total_ht_brut: thtbrut, total_remise: tr, total_ttc: ttc},
    ];
    const rowsTotaux = lignesTotauxGrid;

    // getModalStyle is not a pure function, we roll the style only on the first render
    const [modalStyle] = useState(getModalStyle);
    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    function rand() {
        return Math.round(Math.random() * 20) - 10;
    }
    function getModalStyle() {
        const top = 50 + rand();
        const left = 50 + rand();

        return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
        };
    }
    const useStyles = makeStyles((theme) => ({
        paper: {
            position: 'absolute',
            width: 700,
            backgroundColor: theme.palette.background.paper,
            border: '2px solid #000',
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
        },
    }));
    const classes = useStyles();

    const body = (
        <div style={modalStyle} className={classes.paper}>
            {partNumber()}
        </div>
    );

    return(
        <div>
            <Header />
            {!state.connected &&
                <Disconnect />
            }
            {state.connected && 
                <main className="main_page">
                    <Nav />
                    <ToastContainer />
                    {statusDocument === "loading" && <div>Loading...</div>}
                    {statusDocument === "error" && <div>Error fetching document</div>}
                    {statusDocument === "success" && 
                        <div className="div_main_page">
                            {queryStatuts.status === "success" && tableDocument(document.data[0], queryStatuts.data.data)}
                            <Modal
                                open={open}
                                onClose={handleClose}
                                aria-labelledby="simple-modal-title"
                                aria-describedby="simple-modal-description"
                            >
                                {body}
                            </Modal>
                            <div style={{ width: '97%', margin: '2rem' }}>
                                <Button variant="contained" color="primary" type="button" onClick={handleSavePdf}>
                                    Télécharger le pdf
                                </Button>
                                <Button variant="contained" color="primary" type="button" onClick={handleSendPdf}>
                                    Envoyer
                                </Button>
                                <DataGrid rows={rows} columns={columns} pageSize={5} rowsPerPageOptions={[5, 10, 20]} onEditCellChange={handleEditCellChange} autoHeight={true} rowHeight={100} />  
                                <Button variant="contained" color="primary" type="button" onClick={handleOpen}>
                                    Ajouter
                                </Button>
                            </div>
                            <div style={{ width: '97%', margin: '2rem' }}>
                                <DataGrid rows={rowsTotaux} columns={columnsTotaux} autoHeight={true} />  
                            </div>
                        </div>
                    }
                </main>
            }
            <Footer />
        </div>
    )
}

export default Document;