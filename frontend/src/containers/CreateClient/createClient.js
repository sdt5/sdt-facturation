import Header from "../../components/Header/header";
import Nav from "../../components/Nav/nav";
import Footer from "../../components/Footer/footer";
import { useStore } from '../../store/store';
import { useForm } from 'react-hook-form';
import axios from "axios";
import qs from 'qs';
import * as EmailValidator from 'email-validator';
import { Button, TextField, InputAdornment } from '@material-ui/core';
import { ToastContainer, toast } from 'react-toastify';

const CreateClient = () => {

    // On récurère notre globalState
    const { state } = useStore();

    const { register, handleSubmit, reset } = useForm();

    const onSubmit = async(e) => {

        let clientData = {
            compagny: e.compagny,
            contact_name: e.contact,
            email:e.email, 
            tel: e.tel,
            adress:e.adress,
        }

        let isEmailValid = EmailValidator.validate(e.email);

        if (!isEmailValid) {
            toast("Veuillez saisir un email valide", { type: "error" });
        } else {
            await axios.post(`/api/v1/client/add`, qs.stringify(clientData),
            {
                headers: {
                    'Authorization' : `Bearer ${state.token}`,
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
                }
            }
        )
        .then((res) => {
            if (res.data.error) {
                toast(res.data.error, { type: "error" });
            } else {
                toast("Votre client a été ajouté", { type: "success" });
                reset()
            }
        })
        }
    }


    return(
        <div>
            <Header />
            <main className="main_page">
                <Nav />
                <ToastContainer />
                <div className="table_side">
                    <h2>Ajouter Un client</h2>

                    <form className="form_auth" onSubmit={handleSubmit(onSubmit)}>
                        <TextField 
                        id="outlined-basic" 
                        label="Nom de la société" 
                        inputRef={register}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start" />
                            ),
                            inputRef: register
                        }} 
                        variant="outlined" 
                        required 
                        name="compagny" 
                        register
                        margin="dense"/>

                        <TextField 
                        id="outlined-basic" 
                        label="Nom du contact de la société" 
                        inputRef={register}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start" />
                            ),
                        }} 
                        variant="outlined" 
                        required 
                        name="contact" 
                        margin="dense"/>

                        <TextField 
                        id="outlined-basic" 
                        label="Email" 
                        inputRef={register}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start" />
                            ),
                        }} 
                        variant="outlined" 
                        required 
                        name="email" 
                        margin="dense"/>

                        <TextField 
                        id="outlined-basic" 
                        label="Téléphone" 
                        inputRef={register}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start" />
                            ),
                        }} 
                        variant="outlined" 
                        required 
                        name="tel" 
                        margin="dense"/>

                        <TextField 
                        id="outlined-basic" 
                        label="Adresse" 
                        inputRef={register}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start" />
                            ),
                        }} 
                        variant="outlined" 
                        required 
                        name="adress" 
                        margin="dense"/>

                        <Button type="submit"  variant="contained" color="primary">Ajouter un client</Button>










                        {/* <div className="flex">
                        <label htmlFor="compagny">Nom de la société</label>
                        <input name="compagny" type="text" ref={register} required/>
                        </div>
                        
                        <div className="flex">
                        <label htmlFor="contact_name">Nom du contact de la société</label>
                        <input name="contact_name" type="text" ref={register} required/>
                        </div>
                        

                        <div className="flex">
                        <label htmlFor="email">Email</label>
                        <input name="email" type="text" ref={register} required/>
                        </div>
                        

                        <div className="flex">
                        <label htmlFor="tel">Téléphone</label>
                        <input name="tel" type="text" ref={register} required/>
                        </div>
                        

                        <div className="flex">
                        <label htmlFor="adress">Adresse</label>
                        <input name="adress" type="text" ref={register}/>
                        </div> */}
                        

                        {/* <button type="submit">AJOUTER UN NOUVEAU CLIENT</button> */}
                    </form>
                </div>
            </main>
            <Footer />
        </div>
    )
}

export default CreateClient;