import { useStore } from '../../store/store';
import Header from "../../components/Header/header";
import Footer from "../../components/Footer/footer";
import { useState, useEffect } from 'react';
import axios from "axios";
import { useQuery } from 'react-query';
import { useForm } from 'react-hook-form';
import Nav from "../../components/Nav/nav";
import jwt_decode from "jwt-decode";
import Disconnect from "../../components/Disconnect/disconnect";
import qs from 'qs';
import { Redirect } from 'react-router-dom';

const CreateDocument = () => {

    // On récurère notre globalState
    const { state } = useStore();

    const { register, handleSubmit } = useForm();

    const [type, setType] = useState("DEVIS");
    const [allStatus, setAllStatus] = useState([]);
    const [statusByType, setStatusByType] = useState([]);
    const [messageSubmit, setMessageSubmit] = useState({});

    // On récupère les clients selon leur site
    const fetchClients = async() => {
        const { data } = await axios.get(`/api/v1/list/client`, {
            headers: {
                'Authorization' : `Bearer ${state.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
        return data;
    }

    // On récupère tous les types de document
    const fetchTypes = async() => {
        const { data } = await axios.get(`/api/v1/list/type`, {
            headers: {
                'Authorization' : `Bearer ${state.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
        return data;
    }

    // On récupère tous les types statuts
    const fetchStatuts = async() => {
        const { data } = await axios.get('/api/v1/list/Status', {
            headers: {
                'Authorization' : `Bearer ${state.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
        setAllStatus(data.data);
        return data;
    }

    // On récupère les infos du site
    const fetchSite = async() => {
        const { data } = await axios.get(`/api/v1/site/${state.siteID}`, {
            headers: {
                'Authorization' : `Bearer ${state.token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        })
        return data;
    }

    const clientsQuery = useQuery("clients", fetchClients);
    const sitesQuery = useQuery("site", fetchSite);
    useQuery("types", fetchTypes);
    useQuery("statuts", fetchStatuts);

    // Création d'un default state lorsque le composant est monté
    useEffect(() => {
        let defaultStatus = allStatus.filter((statut) => statut.type_id === 1 || statut.type_id === 3)
        setStatusByType(defaultStatus)
    }, [allStatus])

    const formNewDocument = (clients, site) => {

        const handleTypeChange = (e) => {
            let idType = e.target.value;

            if (idType === "1") {
                setType("DEVIS");
                let filtreStatus = allStatus.filter((statut) => statut.type_id === 1 || statut.type_id === 3)
                setStatusByType(filtreStatus);
            }
            if (idType === "2") {
                setType("FACTURE");
                let filtreStatus = allStatus.filter((statut) => statut.type_id === 2 || statut.type_id === 3)
                setStatusByType(filtreStatus);
            }
        }

        // Création du numéro de document
        let date = new Date();
        let year = date.getFullYear();
        let newIncrement = String(site.data[0].increment + 1).padStart(3, '0');

        let createNumber = `${site.data[0].code}-${type}-${year}-${newIncrement}`

        const onSubmit = async (value) => {
            let tokenDecoded = jwt_decode(state.token)

            let documentData = {
                number: createNumber,
                    idClient: value.idClient,
                    idType:value.idType, 
                    idUser: tokenDecoded.id,
                    idSite:value.idSite,
                    idStatut: value.idStatut
            }

            await axios.post(`/api/v1/document/add`, qs.stringify(documentData),
                {
                    headers: {
                        'Authorization' : `Bearer ${state.token}`,
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
                    }
                }
            )
            .then((res) => {
                console.log(res)
                if (res.error) {
                    setMessageSubmit(res.error);
                }
                if (res.data) {
                    setMessageSubmit(res.data);
                }
            })
        }

        return(
            <div>
                {messageSubmit.data &&
                    <Redirect to={{ pathname:"/submited", data: {data: "Votre document à bien été créé"}}} />
                }
                {messageSubmit.error &&
                    <Redirect to={{ pathname:"/submited", data: {data : "Votre document n'a pas pu être créé"}}} />
                }
                
                <h2>Nouveau document n° {createNumber}</h2>

                <form className="form_auth" onSubmit={handleSubmit(onSubmit)}>
                    <div className="flex">
                        <label htmlFor="client">Client</label>
                        <select type="select" name="idClient" id="client" ref={register}>
                            {clients.data.map((client, i) => {
                                return <option value={client.id} key={i}>{client.contact_name} - {client.compagny}</option>
                            })}
                        </select>
                    </div>
                    
                    <div className="flex">
                        <label htmlFor="type">Type</label>
                        <select type="text" name="idType" id="type" ref={register} onChange={handleTypeChange}>
                            <option value="1">Devis</option>
                            <option value="2">Facture</option>
                        </select>
                    </div>
                    
                    <div className="flex">
                        <label htmlFor="statut">Statut</label>
                        <select type="text" name="idStatut" id="statut" ref={register}>
                            {statusByType.map((statut, i) => {
                                return <option value={statut.id} key={i}>{statut.name}</option>
                            })}
                        </select>
                    </div>
                    
                    <div className="flex">
                        <label htmlFor="site">Site</label>
                        <select type="text" name="idSite" id="site" ref={register}>
                            <option value={state.siteID}>{site.data[0].name}</option>
                        </select>
                    </div>
                    
                    <button type="submit">CREER UN NOUVEAU DOCUMENT</button>
                </form>
            </div>
        )
    }

    return(
        <div>
            <Header />
            {!state.connected && 
                <Disconnect />
            }
            {state.connected && 
                <main className="main_page">
                    <Nav />
                    <div className="table_side">
                        {clientsQuery.status === "loading" && <div>Loading...</div>}
                        {clientsQuery.status === "error" && <div>Error fetching client</div>}
                        {clientsQuery.status === "success" &&  sitesQuery.status === "success" && <div>{formNewDocument(clientsQuery.data, sitesQuery.data)}</div>}
                    </div>
                </main>
            }
            <Footer />
        </div>
    )
}

export default CreateDocument;