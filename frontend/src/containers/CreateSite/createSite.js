import Header from "../../components/Header/header";
import Footer from "../../components/Footer/footer";
import Nav from "../../components/Nav/nav";
import { useStore } from '../../store/store';
import { useCreateSiteMutation } from '../../functions/functions';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, TextField, InputAdornment } from '@material-ui/core';


const CreateSite = () => {

    // On récurère notre globalState
    const { state } = useStore();

    const createSiteMutation = useCreateSiteMutation();

    const handleSubmit = e => {
        e.preventDefault()
        createSiteMutation.mutate({name:e.target.name.value}, state.token)
    }

    return(
        <div>
            <Header />
            <ToastContainer />
                <main className="main_page">
                    <Nav />
                    <div className="table_side">
                        <h2>Créer un nouveau site</h2>
                        <form className="form_auth" onSubmit={handleSubmit}>
                            <TextField 
                            id="outlined-basic" 
                            label="Nom du site" 
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start" />
                                ),
                            }} 
                            variant="outlined" 
                            required 
                            name="name" 
                            margin="dense"/>

                            <Button type="submit"  variant="contained" color="primary">Ajouter un nouveau site</Button>
                        </form>
                    </div>
                </main>
            <Footer />
        </div>
    )
}

export default CreateSite;