import Nav from "../../components/Nav/nav";
import { useStore } from '../../store/store';
import Header from "../../components/Header/header";
import Footer from "../../components/Footer/footer";
import Disconnect from "../../components/Disconnect/disconnect";
import { Link } from 'react-router-dom';
import { useUsers, useDeleteUserMutation } from '../../functions/functions';
import { DataGrid } from '@material-ui/data-grid';
import { Button } from '@material-ui/core';
import { ToastContainer } from 'react-toastify';

const Users = () => {

    // On récurère notre globalState
    const { state } = useStore();

    const { data: users, status: statusUsers } = useUsers(state.token);
    const deleteUserMutation = useDeleteUserMutation();

    const handleClicDeleteUser = async(user) => {
        let paramsQR = {
            token: state.token,
            id: user.value
        }
        deleteUserMutation.mutate(paramsQR)
    }

    const columns = [
        { field: 'id', headerName: 'ID', flex: 0.3, hide: true},
        { field: 'name', headerName: 'Nom', flex: 1, editable: false },
        { field: 'email', headerName: 'Email', flex: 1, editable: false },
        {
            field: 'action',
            headerName: 'Action',
            renderCell: (e) => (
                <Button variant="contained" color="secondary" onClick={() => {handleClicDeleteUser(e)}}>
                    Supprimer
                </Button>
            ),
            flex: 0.5
        },
    ]

    const rows = statusUsers === 'success' ? users.data.map((item, i) => {
        return(
            {
                id: i+1,
                name: item.name,
                email: item.email,
                action: item.id
            }
        )
    }) : [];

    return(
        <div>
            <Header />
            {state.connected && 
            
                <div>
                    <main className="main_page">
                        <Nav />
                        <ToastContainer />
                        {statusUsers === 'loading' && <div>Loading ...</div> }
                        {statusUsers === 'success' &&
                            <div className="table_side">
                                <div style={{ width: '95%', margin: '2rem' }}>
                                    <Button variant="contained" color="primary"><Link to="/ajout/utilisateur">Créer un nouvel utilisateur</Link></Button>
                                    <DataGrid rows={rows} columns={columns} autoHeight={true}  pageSize={5}/>  
                                </div>
                            </div>
                        }
                    </main>
                </div>
            }
            {!state.connected && 
                <Disconnect />
            }
            <Footer />
        </div>
    )
}

export default Users;