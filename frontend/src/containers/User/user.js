import Header from "../../components/Header/header";
import Footer from "../../components/Footer/footer";
import { useStore } from '../../store/store';
import { useParams } from 'react-router-dom';
import Disconnect from "../../components/Disconnect/disconnect";
import Nav from "../../components/Nav/nav";
import { useQuery } from 'react-query';
import { useForm } from 'react-hook-form';
import { useState } from 'react';
import { findUserFunc } from '../../functions/functions';



const User = () => {

    // On récurère notre globalState
    const { state } = useStore();

    const  [user, setUser]  = useState({});

    let { idUtilisateur } = useParams();
    const { register, handleSubmit } = useForm();

    // On récupère les data d'un user selon son id
    const fetchUser = async() => {
        findUserFunc(idUtilisateur, state.token)
        .then((res) => {
            setUser(res.data.data[0])
        })
    }

    const { status } = useQuery("user", fetchUser);

    const onSubmit = (value) => {
        console.log(value)
        // TODO
    }

    const tableUser = (user) => {
        if (user) {
            return(
                <div>
                <form className="form" onSubmit={handleSubmit(onSubmit)}>
                    <div>
                        <label>Nom</label><input name="name" defaultValue={user.name} ref={register} />
                    </div>
                    <div>
                        <label>Mot de passe actuel</label><input name="pin" ref={register} />
                    </div>
                    <div>
                        <label>Nouveau mot de passe</label><input name="new_pin" ref={register} />
                    </div>
                    <div>
                        <label>Saisir le nouveau mot de passe</label><input name="valid_pin" ref={register} />
                    </div>

                    <button type="submit">MODIFIER LES INFORMATIONS</button>
                </form>
                </div>
            )
        } else {
            return(
                <Disconnect />
            )
        }
    }

    return(
        <div>
            <Header />
            {!state.connected &&
                <Disconnect />
            }
            {state.connected &&
                <main className="main_page">
                    <Nav />
                    {status === "loading" && <div>Loading...</div>}
                    {status === "error" && <div>Error fetching user</div>}
                    {status === "success" && <div>{tableUser(user)}</div>}
                </main>
            }
            <Footer />
        </div>
    )
}

export default User;