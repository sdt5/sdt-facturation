import Nav from "../../components/Nav/nav";
import Header from "../../components/Header/header";
import { useStore } from '../../store/store';
import { Link } from 'react-router-dom';
import Footer from "../../components/Footer/footer";
import Disconnect from "../../components/Disconnect/disconnect";
import { DataGrid } from '@material-ui/data-grid';
import { 
    useClients,
    useDeleteClientMutation
} from '../../functions/functions';
import { ToastContainer } from 'react-toastify';
import { Button } from '@material-ui/core';

const Clients = () => {

    // On récurère notre globalState
    const { state } = useStore();

    const { data: client, status: statusClient } = useClients(state.token);

    const deleteClientMutation = useDeleteClientMutation();

    const handleClicDeleteClient = async(client) => {
        let paramsQR = {
            token: state.token,
            clientID: client.value
        }

        deleteClientMutation.mutate(paramsQR)
    }

    const columns = [
        { field: 'id', headerName: 'ID', flex: 0.3, hide: true},
        { field: 'entreprise', headerName: 'Entreprise', flex: 1, editable: false },
        { field: 'contact', headerName: 'Contact', flex: 1, editable: false },
        { field: 'email', headerName: 'Email', flex: 1, editable: false },
        { field: 'tel', headerName: 'Téléphone', flex: 1, editable: false },
        { field: 'adresse', headerName: 'Adresse', flex: 1, editable: false },
        {
            field: 'detail',
            headerName: 'Détail',
            renderCell: (e) => (
                <Button variant="contained" color="primary">
                    <Link to={`/client/${e.id}`}>Détail</Link>
                </Button>
            ),
            flex: 0.7
        },
        {
            field: 'action',
            headerName: 'Action',
            renderCell: (e) => (
                <Button variant="contained" color="secondary" onClick={() => {handleClicDeleteClient(e)}}>
                    Supprimer
                </Button>
            ),
            flex: 1
        },
    ]

    const rows = statusClient === 'success' ? client.data.map((item, i) => {
        return(
            {
                id: i+1,
                entreprise: item.compagny,
                contact: item.contact_name,
                email: item.email,
                tel: item.tel,
                adresse: item.adress,
                detail: item.id,
                action: item.id
            }
        )
    }) : [];

    return(
        <div>
            <Header />
            {!state.connected && 
                <Disconnect />
            }
            {state.connected && 
                <div>
                    <main className="main_page">
                        <Nav />
                        <ToastContainer />
                        {statusClient === "success" && 
                            <div className="table_side">
                                <div style={{ width: '95%', margin: '2rem' }}>
                                    <Button variant="contained" color="primary"><Link to="/ajout/client">Créer un nouveau Clients</Link></Button>
                                    <DataGrid rows={rows} columns={columns} autoHeight={true} />  
                                </div>
                            </div>
                        }
                    </main>
                </div>
            }
            <Footer />
        </div>
    )
}

export default Clients;