import Footer from "../../components/Footer/footer"
import Nav from "../../components/Nav/nav"
import { useForm } from 'react-hook-form';
import { useStore } from '../../store/store';
import axios from "axios";
import qs from 'qs';
import { Button, TextField, InputAdornment } from '@material-ui/core';
import { ToastContainer, toast } from 'react-toastify';


const CreateUser = () => {

    // On récurère notre globalState
    const { state } = useStore();

    const { register, handleSubmit, reset } = useForm();


    const onSubmit = async(e) => {

        let clientData = {
            name: e.name,
            email: e.email,
            pin:e.pin, 
        }

        if (e.pin !== e.pinValid) {
            toast("Veuillez vérifier vos code pin", { type: "error" });
        } else {
            if (isNaN(e.pin)) {
                toast("Votre code doit être composé de chiffres uniquement", { type: "error" });
            }
            await axios.post(`/api/v1/user/add`, qs.stringify(clientData),
                {
                    headers: {
                        'Authorization' : `Bearer ${state.token}`,
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
                    }
                }
            )
            .then((res) => {
                if (res.data.error) {
                    toast(res.data.error, { type: "error" });
                } else {
                    toast("Votre utilisateur a été ajouté", { type: "success" });
                    reset()
                }
            })
        }
    }



    return(
        <div>
            <main className="main_page">
                <Nav />
                <ToastContainer />
                <div className="table_side">
                    <h2>Créer un nouvel utilisateur</h2>

                    <form className="form_auth" onSubmit={handleSubmit(onSubmit)}>
                        <TextField 
                        id="outlined-basic" 
                        label="Nom" 
                        inputRef={register}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start" />
                            ),
                            inputRef: register
                        }} 
                        variant="outlined" 
                        required 
                        name="name" 
                        register
                        margin="dense"/>

                        <TextField 
                        id="outlined-basic" 
                        label="Email" 
                        inputRef={register}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start" />
                            ),
                        }} 
                        variant="outlined" 
                        required 
                        name="email" 
                        margin="dense"/>

                        <TextField 
                        id="outlined-basic" 
                        label="Code pin" 
                        inputRef={register}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start" />
                            ),
                        }} 
                        variant="outlined" 
                        required 
                        name="pin" 
                        margin="dense"/>

                        <TextField 
                        id="outlined-basic" 
                        label="Veuillez saisir votre code pin" 
                        inputRef={register}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start" />
                            ),
                        }} 
                        variant="outlined" 
                        required 
                        name="pinValid" 
                        margin="dense"/>

                        <Button type="submit"  variant="contained" color="primary">Ajouter un utilisateur</Button>
                    </form>
                </div>
            </main>
            <Footer />
        </div>
    )
}

export default CreateUser;