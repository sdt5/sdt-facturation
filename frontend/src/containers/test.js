import React, { useMemo } from "react";
import { useDeletePokemonMutation, usePokemons } from "./api";
 
export default function App() {
 const { isLoading, error, data, refetch } = usePokemons();
 
 const tempData = useMemo(() => {
 return { ...data };
 }, [data]);
 console.log("🚀 ~ file: App.js ~ line 10 ~ tempData ~ tempData", tempData);
 
 const deletePokemonMutation = useDeletePokemonMutation();
 
 if (isLoading) return "loading...";
 if (error) return "error";
 if (data)
 return (
 <ul>
 {data.results.map((pokemon) => (
 <li key={pokemon.name}>
 {pokemon.name}{" "}
 <button
 style={{ marginLeft: 8 }}
 onClick={(e) => deletePokemonMutation.mutate(pokemon.name)}
 >
 Delete
 </button>
 <button onClick={refetch}>Reload</button>
 </li>
 ))}
 </ul>
 );
}
 
// api.js
import axios from "axios";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { toast } from "react-toastify";
 
export const usePokemons = () => {
 return useQuery("documents", () =>
 axios("https://pokeapi.co/api/v2/pokemon").then((res) => res.data)
 );
};
 
export const useDeletePokemonMutation = () => {
    const queryClient = useQueryClient();
    
    return useMutation((name) => {
            //console.log(`Deleting ${name}`);
            return axios.delete(`https://pokeapi.co/api/v2/pokemon/${name}`);
    },

    {
        onSuccess: () => {
            queryClient.invalidateQueries("documents");
            toast(`Pokemon deleted successfully`, { type: "success" });
        },
        onError: (err) => {
            toast(`error ${err.message}`, { type: "error" });
        },
    }
)};