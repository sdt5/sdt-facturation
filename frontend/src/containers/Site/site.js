import Header from "../../components/Header/header";
import Footer from "../../components/Footer/footer";
import { useStore } from '../../store/store';
import { useQuery } from 'react-query';
import { useParams } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import Disconnect from "../../components/Disconnect/disconnect";
import Nav from "../../components/Nav/nav";
import { findSiteById, findECByIdSite } from '../../functions/functions';

const Site = () => {

    // On récurère notre globalState
    const { state } = useStore();

    let { idSite } = useParams();
    const { register, handleSubmit } = useForm();

    // use query
    const { data, status } = useQuery("document", () => findSiteById(idSite, state.token));
    const listECQuery = useQuery("ec", () => findECByIdSite(idSite, state.token));

    const onSubmit = (value) => {
        // console.log(value)
        // TODO
    }

    const listEC = (ec) => {
        return(
            <div>
                {ec.data.data.map((item) => {
                    return <li>{item.libelle}</li>
                })}
            </div>
        )
    }

    const siteDetail = (ec) => {
        if (data.data) {
            return(
                <div>
                    <h2>Exercices Comptable - {ec.name}</h2>

                    {listECQuery.status === "success" && <div>{listEC(listECQuery.data)}</div>}

                    <form className="form" onSubmit={handleSubmit(onSubmit)}>
                        <label htmlFor="start">Start date:</label>
                        <input 
                            type="date" 
                            id="start" 
                            name="date_debut"
                            ref={register}
                        />
                        <label htmlFor="end">End date:</label>
                        <input 
                            type="date" 
                            id="end" 
                            name="date_fin"
                            ref={register}
                        />
                            
                        <button type="submit">AJOUTER UN EXERCICE COMPTABLE POUR LE SITE DE {ec.name}</button>
                    </form>
                </div>
            )
        } else {
            return(
                <Disconnect />
            )
        }
    }

    return(
        <div>
            <Header />
            {!state.connected &&
                <Disconnect />
            }
            {state.connected &&
                <main className="main_page">
                    <Nav />
                    {status === "success" && <div>{siteDetail(data.data.data[0])}</div>}
                </main>
            }
            <Footer />
        </div>
    )
}

export default Site;