import Nav from "../../components/Nav/nav";
import { useStore } from '../../store/store';
import Header from "../../components/Header/header";
import { useState, useMemo } from 'react';
import Footer from "../../components/Footer/footer";
import Disconnect from "../../components/Disconnect/disconnect";
import { Link } from 'react-router-dom';
import { listTVAFunc, useTVAS, useDeleteTvaMutation } from '../../functions/functions';
import { ToastContainer } from 'react-toastify';
import { DataGrid } from '@material-ui/data-grid';
import { Button } from '@material-ui/core';

const TVA = () => {

    // On récurère notre globalState
    const { state } = useStore();

    const [listTVA, setListTVA] = useState([]);

    const { data: tvas, status: statusTvas } = useTVAS(state.token);

    const deleteTvaMutation = useDeleteTvaMutation();


    const handleClicDeleteTVA = (e) => {
        let paramsQR = {
            id: e.value,
            token: state.token,
        }

        deleteTvaMutation.mutate(paramsQR)
    }

    const columns = [
        { field: 'id', headerName: 'ID', flex: 0.3, hide: true},
        { field: 'name', headerName: 'Nom', flex: 1, editable: false },
        { field: 'taux', headerName: 'Taux', flex: 1, editable: false },
        {
            field: 'action',
            headerName: 'Action',
            renderCell: (e) => (
                <Button variant="contained" color="secondary" onClick={() => {handleClicDeleteTVA(e)}}>
                    Supprimer
                </Button>
            ),
            flex: 0.5
        },
    ]

    const rows = statusTvas === 'success' ? tvas.data.map((item, i) => {
        return(
            {
                id: i+1,
                name: item.name,
                taux: item.taux,
                action: item.id
            }
        )
    }) : [];

    return (
        <div>
            <Header />
            {!state.connected && 
                <Disconnect />
            }
            {state.connected && 
            
                <div>
                    <main className="main_page">
                        <Nav />
                        <ToastContainer />
                        {statusTvas === 'success' &&
                            <div className="table_side">
                                <div style={{ width: '95%', margin: '2rem' }}>
                                    <Button variant="contained" color="primary"><Link to="/ajout/tva">Créer un nouveau taux</Link></Button>
                                    <DataGrid rows={rows} columns={columns} autoHeight={true}  pageSize={5}/>  
                                </div>
                            </div>
                        }
                    </main>
                </div>
            }
            <Footer />
        </div>
    )
}

export default TVA;