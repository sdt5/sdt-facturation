import Header from "../../components/Header/header";
import Footer from "../../components/Footer/footer";
import Nav from "../../components/Nav/nav";
import { useStore } from '../../store/store';
import { Link } from 'react-router-dom';
import { useCreateStatusMutation } from '../../functions/functions';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, TextField, InputAdornment, FormControl, MenuItem, InputLabel, Select } from '@material-ui/core';

const CreateStatus = () => {

    // On récurère notre globalState
    const { state } = useStore();

    const createStatusMutation = useCreateStatusMutation();

const handleSubmit = e => {
    e.preventDefault()
    createStatusMutation.mutate({name:e.target.name.value, type:e.target.type.value, token: state.token})
}

    return(
        <div>
            <Header />
            <ToastContainer />
                <main className="main_page">
                    <Nav />
                    <div className="table_side">
                        <h2>Créer un nouveau statut</h2>
                        <form className="form_auth" onSubmit={handleSubmit}>

                            <TextField 
                            id="outlined-basic" 
                            label="Nom du statut" 
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start" />
                                ),
                            }} 
                            variant="outlined" 
                            required 
                            name="name" 
                            margin="dense"/>
                            <FormControl 
                            variant="outlined" 
                            required 
                            margin='dense'
                            label="Veuillez selectionner un site pour vous connecter">
                                <InputLabel id="demo-simple-select-outlined-label" variant="outlined" >Ce statut concerne</InputLabel>
                                <Select
                                labelId="demo-simple-select-outlined-label"
                                id="demo-simple-select-outlined"
                                name="type"
                                label="Ce statut concerne"
                                >
                                    <MenuItem value="1">Un devis</MenuItem>
                                    <MenuItem value="2">Une facture</MenuItem>
                                    <MenuItem value="3">Les deux</MenuItem>

                                </Select>
                            </FormControl>

                            <Button type="submit"  variant="contained" color="primary">Ajouter un nouveau site</Button>
                        </form>
                    </div>
                </main>
            <Footer />
        </div>
    )
}

export default CreateStatus;