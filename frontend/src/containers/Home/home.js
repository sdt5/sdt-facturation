import Header from "../../components/Header/header";
import Footer from "../../components/Footer/footer";
import Form from "../../components/Form/form";
import './style.css';

const Home = () => {

    return(
        <div>
            <Header />
                <h1 className="h1_home">Rédigez simplement vos devis, factures, et autre document administratif.</h1>
                <Form />
            <Footer />
        </div>
    )
}

export default Home;