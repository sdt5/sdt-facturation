import Header from "../../components/Header/header";
import Footer from "../../components/Footer/footer";
import Nav from "../../components/Nav/nav";
import { useStore } from '../../store/store';
import { Link } from 'react-router-dom';
import { useAddTvaMutation } from '../../functions/functions';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, TextField, InputAdornment, FormControl, MenuItem, InputLabel, Select } from '@material-ui/core';

const CreateTva = () => {

    // On récurère notre globalState
    const { state } = useStore();

    const addTvaMutation = useAddTvaMutation();

const handleSubmit = e => {
    e.preventDefault()
    addTvaMutation.mutate({name:e.target.name.value, taux:e.target.taux.value, token: state.token})
}

    return(
        <div>
            <Header />
            <ToastContainer />
                <main className="main_page">
                    <Nav />
                    <div className="table_side">
                        <h2>Créer un nouveau statut</h2>
                        <form className="form_auth" onSubmit={handleSubmit}>
                            <TextField 
                            id="outlined-basic" 
                            label="Nom" 
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start" />
                                ),
                            }} 
                            variant="outlined" 
                            required 
                            name="name" 
                            margin="dense"/>
                            <TextField 
                            id="outlined-basic" 
                            label="Taux" 
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start" />
                                ),
                            }} 
                            variant="outlined" 
                            required 
                            name="taux" 
                            margin="dense"/>

                            <Button type="submit"  variant="contained" color="primary">Ajouter un taux</Button>






                            {/* <div className="flex">
                                <label htmlFor="name">Nom du statut</label>
                                <input name="name" type="text" required/>
                            </div>
                            <div className="flex">
                                <label htmlFor="type">Ce statut concerne</label>
                                <select name="type" id="type">
                                    <option value="1">Un devis</option>
                                    <option value="2">Une facture</option>
                                    <option value="3">Les deux</option>
                                </select>
                            </div>

                            <button type="submit">AJOUTER UN NOUVEAU STATUT</button> */}
                        </form>
                    </div>
                </main>
            <Footer />
        </div>
    )
}

export default CreateTva;