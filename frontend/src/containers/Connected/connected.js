import Header from "../../components/Header/header";
import Footer from "../../components/Footer/footer";
import './style.css';
import { useStore } from '../../store/store';
import { Link } from 'react-router-dom';
import Nav from "../../components/Nav/nav";
import Disconnect from "../../components/Disconnect/disconnect";
import { DataGrid } from '@material-ui/data-grid';
import { Button } from '@material-ui/core';
import { useDocuments } from '../../functions/functions';

const Connected = () => {

    // On récurère notre globalState
    const { state } = useStore();

    const { data: documents, status: statusDocuments } = useDocuments(state.siteID, state.token);

    const columns = [
        { field: 'id', headerName: 'ID', flex: 0.3 },
        { field: 'number', headerName: 'Numéro', flex: 1 },
        { field: 'client', headerName: 'Client', flex:1 },
        { field: 'type', headerName: 'Type', flex: 0.5 },
        {
          field: 'statut',
          headerName: 'Statut',
          type: 'number',
          flex: 0.5,
        },
        {
          field: 'revision',
          headerName: 'Révision',
          description: 'This column has a value getter and is not sortable.',
          sortable: false,
          type: 'number',
          flex: 0.5,
        },
        {
            field: 'created_at',
            headerName: 'Date de création',
            description: 'This column has a value getter and is not sortable.',
            sortable: false,
            flex: 1,
            type: 'date',
        },
        {
            field: 'updated_at',
            headerName: 'Dernière modification',
            description: 'This column has a value getter and is not sortable.',
            sortable: false,
            flex: 1,
        },
        {
            field: 'action',
            headerName: 'Action',
            renderCell: (e) => (
              <Button variant="contained" color="primary">
                <Link to={`document/${e.value}`}>Détail</Link>
              </Button>
            ),
          },
      ];

    const documentGrid = statusDocuments === 'success' ? documents.data.map((document, i) => {
        return( 
            {
                id: i+1, 
                number: document.number,
                client: document.compagny,
                type: document.type,
                statut: document.statut,
                revision: document.revision,
                created_at: document.created_at,
                updated_at: document.updated_at,
                action: document.id
            }
        )
    }) : [];
    const rows = documentGrid

    return(
        <div>
            <Header />
            {!state.connected && 
                <Disconnect />
            }
            {state.connected && 
            
                <div>
                    <main className="main_page">
                        <Nav />
                        {statusDocuments === 'success' &&
                            <div className="table_side">
                                <div style={{ height: 400, width: '95%', margin: '2rem auto' }}>
                                    <Button variant="contained" color="primary"><Link to="/ajout/document">Créer un nouveau document</Link></Button>
                                    <DataGrid rows={rows} columns={columns} pageSize={5} autoHeight={true} />  {/** checkboxSelection */}
                                </div>
                            </div>
                        }

                    </main>
                </div>
            }
            <Footer />
        </div>
    )
}

export default Connected;