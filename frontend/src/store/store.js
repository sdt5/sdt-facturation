import { createContext, useContext, useReducer } from 'react';

const StoreContext = createContext();

const initialState = {
    token: localStorage.token,
    connected: localStorage.connected,
    siteID: localStorage.siteID
}

const reducer = ((state, action) => {
    switch(action.type) {
        case 'TOKEN':
            return {
                ...state,
                token: action.token
            }
        case 'CONNECTED':
            return {
                ...state,
                connected: action.connected
            }
        case 'SITE':
                return {
                    ...state,
                    siteID: action.siteID
                }
        default:
            throw new Error(`Unhandled action type: ${action.type}`)
    }
})

export const StoreProvider = ({children}) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    return(
        <StoreContext.Provider value={{state, dispatch}}>
            {children}
        </StoreContext.Provider>
    )
}

export const useStore = () => useContext(StoreContext);