import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { StoreProvider } from './store/store';
import Home from './containers/Home/home';
import Connected from './containers/Connected/connected';
import Clients from './containers/Clients/clients';
import Users from './containers/Users/users';
import Sites from './containers/Sites/sites';
import Statuts from './containers/Status/statuts';
import TVA from './containers/TVA/tva';
import Document from './containers/Document/document';
import Client from './containers/Client/client';
import User from './containers/User/user';
import { QueryClient, QueryClientProvider } from 'react-query';
import { MuiThemeProvider, createMuiTheme, withStyles } from '@material-ui/core'
import Site from './containers/Site/site';
import CreateDocument from './containers/CreateDocument/createDocument';
import Add from './containers/Add/add';
import CreateClient from './containers/CreateClient/createClient';
import CreateUser from './containers/CreateUser/createUser';
import CreateSite from './containers/CreateSite/createSite';
import CreateStatus from './containers/CreateStatus/createStatus';
import styles from './style';
import CreateTva from './containers/createTva/createTva';


 // Create a client
 const queryClient = new QueryClient()

function App() {
  return (
    <div className="App">
      <Router>
        <StoreProvider>
        <QueryClientProvider client={queryClient}>
        <MuiThemeProvider theme={theme} >
          <Switch>
            <Route exact path="/" >
              <Home />
            </Route>
            <Route path="/profil">
              <Connected />
            </Route>
            <Route path="/document/:idDocument">
              <Document />
            </Route>
            <Route path="/ajout/document">
              <CreateDocument />
            </Route>
            <Route path="/submited">
              <Add />
            </Route>
            <Route path="/clients">
              <Clients />
            </Route>
            <Route path="/client/:idClient">
              <Client />
            </Route>
            <Route path="/ajout/client">
              <CreateClient />
            </Route>
            <Route path="/utilisateurs">
              <Users />
            </Route>
            <Route path="/utilisateur/:idUtilisateur">
              <User />
            </Route>
            <Route path="/ajout/utilisateur">
              <CreateUser />
            </Route>
            <Route path="/sites">
              <Sites />
            </Route>
            <Route path="/site/:idSite">
              <Site />
            </Route>
            <Route path="/ajout/site">
              <CreateSite />
            </Route>
            <Route path="/statuts">
              <Statuts />
            </Route>
            <Route path="/ajout/statut">
              <CreateStatus />
            </Route>
            <Route path="/tva">
              <TVA />
            </Route>
            <Route path="/ajout/tva">
              <CreateTva />
            </Route>
          </Switch>
          </MuiThemeProvider>
          </QueryClientProvider>
        </StoreProvider>
      </Router>
    </div>
  );
}

const theme = createMuiTheme({
  overrides : {
    MuiSelect: {
      select: {
        color: 'black',
        fontSize: '1.3rem'
      }
    },
    MuiInputLabel : {
      root : {
        color: 'black'
      }
    },
    MuiInputBase : {
      root : {

      }
    }
  }
})

export default withStyles(styles)(App);
